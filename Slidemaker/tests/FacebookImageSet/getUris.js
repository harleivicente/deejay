var expect = require('chai').expect;
var FacebookImageSetModel = require("../../FacebookImageSet/FacebookImageSetModel");
var FacebookImageSet = require("../../FacebookImageSet");
var async = require("async");
var testUtility = require("./testUtility");

describe('FacebookImageSet getUris()', function() {

	beforeEach(function(done){
		testUtility.clearAllFbImageSets(function(error){
			expect(error).to.be.null;
			done();
		})
	})

	afterEach(function(done){
		testUtility.clearAllFbImageSets(function(error){
			expect(error).to.be.null;
			done();
		})
	})
	
	it("It returns unknown-error if image set does not exist", function(done){
		FacebookImageSet.getUris("fakeId", function(error, uris){
			expect(error).to.be.an("error");
			expect(error.message).to.equal("unknown-error");
			expect(uris).to.be.null;
			done();
		})
	});

	it("It returns valid uris if valid args given", function(done){
		var options = {
			facebookUserId: testConfig.fbUserId,
			images: testConfig.fbImages
		};

		FacebookImageSet.create(options, function(error, imageSetId){
			expect(error).to.be.null;

			FacebookImageSet.getUris(imageSetId, function(error, uris){
				expect(error).to.be.null;
				expect(uris.length).to.equal(testConfig.fbImages.length);
				for(var i =0; i < testConfig.fbImages.length; i++){
					var valid = appUrlRoot + "slide_data/facebook_images/" + imageSetId + testConfig.fbImages[i];
					expect(uris[i]).to.equal(valid);
				}
				done();
			});

		});
	});
});

