/*

 ___________________________________
|							  		|
|				Debugger	  	 	|
|___________________________________|
|ZERO								|
|									|
|									|
|				STAGE				|
|									|
|									|
|									|
|									|
|___________________________________|

	D - Beat indicator - 100px by 100px



*/
function Dimensions(canvas, debug) {
	this.canvas = canvas;
	this.debug = debug;

	// CONFIG
	this.config = {
		image_frame_border: {perc: [1,1,1,1]}
	};
	
	// CACHE
	this.cache = {};
}

Dimensions.prototype.fetch = function(value){
	if(this.cache[value]) {
		return this.cache[value];
	} else {
		var result = this[value]();
		this.cache[value] = result;
		return result;
	}
}

Dimensions.prototype.invalidade_cache = function(){
	this.cache = {};
	this.unvalidated_cache = false;
}

/*
	STAGE
*/
Dimensions.prototype.stage_zero = function(){
	if(this.debug)
		return {x: 0, y: 100};
	else
		return {x: 0, y: 0};
}

Dimensions.prototype.stage_width = function(){
	return this.canvas.width;
}

Dimensions.prototype.stage_height = function(){
	if(this.debug)
		return this.canvas.height - 100;
	else
		return this.canvas.height;
}

/*
	BEAT INDICATOR / DEBUGGER AREA
*/
Dimensions.prototype.beat_indicator_zero = function(){
	return {x: 0, y: 0};
}
Dimensions.prototype.beat_indicator_width = function(){
	return this.canvas.width;
}
Dimensions.prototype.beat_indicator_height = function(){
	return 100;
}


/*
	FRAME
*/
Dimensions.prototype.frame_zero = function(){
	var xplus = this.stage_width() * this.config.image_frame_border.perc[3] / 100;
	var yplus = this.stage_height() * this.config.image_frame_border.perc[0] / 100;
	var stage_zero = this.stage_zero();
	return {x: stage_zero.x + xplus, y: stage_zero.y + yplus};
}

Dimensions.prototype.frame_width = function(){
	var padding_x_total = this.config.image_frame_border.perc[1] + this.config.image_frame_border.perc[3];
	return (1 - (padding_x_total / 100)) * this.stage_width();
}

Dimensions.prototype.frame_height = function(){
	var padding_y_total = this.config.image_frame_border.perc[0] + this.config.image_frame_border.perc[2];
	return (1 - (padding_y_total / 100)) * this.stage_height();
}

Dimensions.prototype.frame_ratio = function(){
	var width = this.fetch("frame_width");
	var height = this.fetch("frame_height");
	return (width/height).toFixed(1);
}
