/*
	@param dev boolean - if in debugger mode
	@param playerApp object React object of the app root

*/
function Manager(dev, playerRoot) {
	this.dev_mode = dev;
	this.playerRoot = playerRoot;

	// Capture trim from html
	this.trim_start_ms = JSON.parse($("input[name='trim-start']").val());
	this.trim_end_ms = JSON.parse($("input[name='trim-end']").val());
	this.trim_start = Math.round(this.trim_start_ms/1000);
	this.trim_end = Math.round(this.trim_end_ms/1000);

	var set = JSON.parse($("input[name='keyframe-set']").val());
	this.keyframeSet = new KeyframeSet(set, this.trim_start_ms);
	this.images = new Images();
	this.animator = new Animator(this);

	if(this.dev_mode){
		var beatData = JSON.parse($("input[name='beats']").val());
		this.beatSet = new BeatSet(beatData, this.trim_start_ms);
		this.beatTracker = new BeatTracker(this);
	}
}

Manager.prototype.time = function() {
	return audio_player.currentTime * 1000;
}

Manager.prototype.construct = function() {
	var instance = this;

	instance.canvas = $("#canvas")[0];
	instance.context2d = instance.canvas.getContext("2d");
	instance.rekapi = new Rekapi(instance.context2d);
	instance.dimensions = new Dimensions(instance.canvas, instance.dev_mode);
	instance.printer = new Printer(instance.context2d);

	// Configure canvas resize loop
	setInterval(instance.canvas_resize, 10);
	setInterval(instance.animator.update, 5);
	if(this.dev_mode){
		setInterval(instance.beatTracker.update, 1);
	}
	
	// Create image stage
	instance.createImageStage();
	if(instance.dev_mode){
		instance.createBeatIndicatorStage();
	}

	$("#playback").removeClass("hide");
	manager.register_control_events();
}

/*
	Register handlers for show controller.
*/
Manager.prototype.register_control_events = function() {
	var play = $("#player-controls span.play");
	var pause = $("#player-controls span.pause");
	var repeat = $("#player-controls span.repeat");
	var make_new = $("#player-controls span.new");
	var share = $("#player-controls span.share");
	var post_button = $("#post-facebook");
	var instance = this;

	share.click(function(e){
		// pause the slide first
		pause.addClass('hide');
		play.removeClass('hide');
		audio_player.pause();
		manager.rekapi.stop();	
		
		// Show modal
		$('#slide-share').modal({
			show: true
		});
	});

	post_button.click(function(e){
		FB.ui({
			method: 'share',
			redirect_uri: "192.168.147.128",
			href: window.location.href,
		}, function(response){
			console.log(response);
		});
	})

	$("#slide-link").on('mouseup', function() { $(this).select(); });

	play.click(function(e){
		play.addClass('hide');
		pause.removeClass('hide');
		audio_player.play();
		manager.rekapi.play();
	});

	pause.click(function(e){
		pause.addClass('hide');
		play.removeClass('hide');
		audio_player.pause();
		manager.rekapi.stop();
	});

	repeat.click(function(e){
		audio_player.currentTime = instance.trim_start;
		manager.animator.reset();
		manager.beatTracker.reset();
		// play in case it is paused
		play.addClass('hide');
		pause.removeClass('hide');
		audio_player.play();
		manager.rekapi.play();
	});

	make_new.click(function(e){
		window.location = "/build";
	});
}

/*
	Loads audio data from server. Audio uri
	should be present in <input name=audio-uri...>

	@param callback function(error)

*/
Manager.prototype.load_audio = function(callback) {
	var audio_uri = $("input[name='audio-uri']").val();
	var instance = this;

	if(!audio_uri){
		callback(true);
	} else {
		audio_player.onloadeddata = function(){
			callback(false);
		}
		audio_player.src = audio_uri;
		audio_player.currentTime = instance.trim_start;

		// check loop if song ended
		setInterval(function(){
			var difference = Math.abs(audio_player.currentTime - instance.trim_end);
			if(difference <= 0.1){
				instance.ended_song();
			}
		},1);
	}
}


/*
	Verifies if server sent any erro information
	on slide via <input data=exception/slide_removed...>

	@param callback function(removed, exception)
		removed - bool
		exception - bool
*/
Manager.prototype.check_server_side_error = function(callback) {
	var exception = $("input[name='exception']").val();
	var removed = $("input[name='slide-removed']").val();
	callback(removed == 1, exception == 1);
}


/*
	Loads images. Image uris should be in 
	<input name=image-uris...>

	@param callback function(error)

*/
Manager.prototype.load_images = function(callback) {
	var data = $("input[name='image-uris']").val();
	var instance = this;

	if(!data){
		callback(true);
	} else {
		var fail = false;

		try{
			var parsed = JSON.parse(data);
		} catch(e){
			fail = true;
		}

		if(fail){
			callback(true);
		} else {
			for(var k in parsed) {
				var image = new Image();
				image.src = parsed[k];		
		    	instance.images.solo_images.push(image);
			}
			callback(false);
		}
	}	
}

Manager.prototype.ended_song = function(){
	var play = $("#player-controls span.play");
	var pause = $("#player-controls span.pause");
	pause.addClass('hide');
	play.removeClass('hide');
	manager.rekapi.stop();
	audio_player.pause();
	audio_player.currentTime = this.trim_start;
	manager.animator.reset();
	manager.beatTracker.reset();
}

Manager.prototype.canvas_resize = function() {
  var canvas_html = $("#canvas_holder");

  if(	
  		manager.canvas.width != Math.round(canvas_html.width()) ||
  		manager.canvas.height != Math.round(canvas_html.height())
	){
	    manager.canvas.width = Math.round(canvas_html.width());
	    manager.canvas.height = Math.round(canvas_html.height());
	    manager.dimensions.invalidade_cache();
	    manager.animator.update();
	    manager.rekapi.update();
  }
}

Manager.prototype.createImageStage = function(){
	var actor = new Rekapi.Actor(
		{
			'render': 
				function (context, state) {
					manager.printer.printStageBg();
					var scheme = manager.animator.scheme_of_images();
					manager.printer.printImages(scheme);
				}
		}
	);
	manager.rekapi.addActor(actor);
}

Manager.prototype.createBeatIndicatorStage = function(){
	var actor = new Rekapi.Actor(
		{
			'render': 
				function (context, state) {
					manager.printer.printBeatIndicator();
				}
		}
	);
	manager.rekapi.addActor(actor);
}


