var functionB = function(callback){
	callback(false);
}


function mainFn(number, callback){
	functionB(function(err){
		if(number == 1)
			callback(err);
		else
			functionB(callback);
	});
}

module.exports.mainFn = mainFn;