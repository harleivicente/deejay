var expect = require('chai').expect;
var path = require("path");
var fs = require("fs");
var rewire = require("rewire");
var YoutubeAudio = rewire("../../YoutubeAudio");
var YoutubeAudioModel = require("../../YoutubeAudio/YoutubeAudio");

describe('YoutubeAudio isReady()', function() {

	before(function(done){
		YoutubeAudio.__set__("YoutubeDownloader", youtubeDownloaderStub);
		YoutubeAudio.__set__("AudioAnalyzer", youtubeAnalyzerStub);
		YoutubeAudio.__set__("YoutubeHouseKeeper", youtubeHouseKeeperStub);
		YoutubeAudio.__set__("SingleImageAnimator", singleImageAnimatorStub);
		done();
	});

	beforeEach(function(done){
		YoutubeAudioModel.remove({}, function(error){
			expect(error).to.be.null;
			done();
		});
	});

	afterEach(function(done){
		YoutubeAudioModel.remove({}, function(error){
			expect(error).to.be.null;
			done();
		});
	});

	it("Returns false for audio that does not exist", function(done){
		YoutubeAudio.isReady("fakeid", function(error, ready){
			expect(error).to.be.null;
			expect(ready).to.be.false;
			done();
		})
	});

	it("Returns false for audio that is currently being analyzed", function(done){
		YoutubeAudioModel.insert("down-ok-analyze-ok", function(error, audio_id){
			expect(error).to.be.null;
			YoutubeAudio.isReady(audio_id, function(error, ready){
				expect(error).to.be.null;
				expect(ready).to.be.false;
				done();
			})
		});
	});
	
	it("Returns false for audio that is invalid", function(done){
		YoutubeAudio.analyze("down-ok-analyze-invalid", function(error, id){
			YoutubeAudio.isReady(id, function(error, ready){
				expect(error).to.be.null;
				expect(ready).to.be.false;
				done();
			});
		});		
	});

	it("Returns true for audio that is valid", function(done){
		YoutubeAudio.analyze("down-ok-analyze-ok", function(error, id){
			expect(error).to.be.null;
			YoutubeAudio.isReady(id, function(error, ready){
				expect(error).to.be.null;
				expect(ready).to.be.true;
				done();
			});
		});		
	});

});


var youtubeDownloaderStub = {
	download: function(video_code, video_code, callback) {
		if(video_code.indexOf("down-ok") >= 0) {
			callback(null);
		} else {
			callback(new Error("unknown-error"));
		}
	}
};

var youtubeAnalyzerStub = {
	analyze: function(file, callback){
		setTimeout(function(){
			if(file.indexOf("analyze-ok") >= 0){
				callback(null, '{"energy":[0.24,0.78],"rate_of_energy":[0.1,0.2]}');
			} else if (file.indexOf("analyze-invalid") >= 0){
				callback(new Error("invalid-file"));
			} else {
				callback(new Error("unknown-error"));
			}
		}, file.indexOf("delayed") >= 0 ? (testConfig.delayIncremet * 10) : 0);
	}
};

/*
	Attempts:
		1) remove entry from db where videoCode is <video code>
		2) remove <video code>.* from ./public/youtube/
*/
var youtubeHouseKeeperStub = {
	clean: function(video_code, callback){
		callback(null);
	}
}


var singleImageAnimatorStub = function(data){
	this.data = data;
};

singleImageAnimatorStub.prototype.buildAllKeyframeSets = function(){
	if(this.data.energy)
		return [
			[{t:2}],
			[{t:2}, {t:2}],
			[{t:2},{t:2},{t:2},{t:2},{t:2},{t:2},{t:2}] // 7
		]
	else
		return false;
}
