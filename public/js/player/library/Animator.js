/*
	Responsible for animating the image.
*/
function Animator(manager) {
	this.manager = manager;
	
	/* 
		Index of image that should be rendered
	*/
	this.image_index = null;

	/*
		Index of manager.keyframes that
		should be processed next
	*/
	this.current_keyframe = null; 

	/*
		Keyframes that were skipped
	*/
	this.skipped_keyframes = null;

	/*
		How close to keyframe time must be
		to trigger it in ms.
	*/
	this.trigger_threshold = 60;

	/*
		Attributes used to calculate average error
		of keyframe use. Only in dev mode
	*/
	this.current_total_error = null;
	this.current_number_keyframe = null;

	this.reset(manager);
}

/*
	Prepares animator to restart animation
	from soundtrack initial trim point.
*/
Animator.prototype.reset = function(){
	this.image_index = 0;
	this.current_keyframe = this.manager.keyframeSet.initial_keyframe;
	this.current_total_error = 0;
	this.current_number_keyframe = 0;
	this.skipped_keyframes = 0;

	if(this.manager.playerRoot.state.debugger){
		this.manager.playerRoot.state.debugger.setState({
			avg_keyframe_error: this.current_total_error / this.current_number_keyframe,
			skipped_keyframes: this.skipped_keyframes
		});	
	}
}

Animator.prototype.update = function() {
	manager.animator.update_image_index();
}

Animator.prototype.current_img_index = function() {
	return this.image_index;
}

/*
	Updates:
		this.image_index
		this.current_keyframe		

	Once there are no more keyframes wont do anything.
*/
Animator.prototype.update_image_index = function() {
	var currentKeyTime = manager.keyframeSet.getKeyframeTime(this.current_keyframe);
	var now = manager.time();

	// Reached end of keyframe set
	if(currentKeyTime === false){
		
	// More keyframes to do
	} else {
		/*
			If positive keyframe is in future
		*/
		var deltaTime = currentKeyTime - now;
		var futureKeyframe = deltaTime > 0;

		/*
			Trigger keyframe
		*/
		if(Math.abs(deltaTime) <= this.trigger_threshold){
			this.image_index++;
			this.current_keyframe++;

			if(this.manager.dev_mode){
				this.current_number_keyframe++;
				var error = Math.abs(deltaTime);
				this.current_total_error += error;
				var updated_value = (this.current_total_error/this.current_number_keyframe);
				updated_value = Math.round(updated_value);
				// Update debugger
				this.manager.playerRoot.state.debugger.setState({
					avg_keyframe_error: updated_value
				});
			}

		/*
			Check if keyframe is too far in the past
		*/
		} else if (!futureKeyframe) {
			this.current_keyframe++;

			if(this.manager.dev_mode){
				this.skipped_keyframes++;
				this.manager.playerRoot.state.debugger.setState({
					skipped_keyframes: this.skipped_keyframes
				});
			}
		}
	}
}

/*
	Obtains a list of the images to render in the format:
		array of:
		{
			i: index of image in this.data,
			img_x: <>, img_y: <>, img_width: <>, img_height: <>,
			x: <>, y: <>,
			width: <>, height: <>
		}	
*/
Animator.prototype.scheme_of_images = function() {
	var img_r = manager.images.solo_images[this.image_index];

	if(img_r){
		var img_ratio = img_r.width / img_r.height;

		if(img_ratio > manager.dimensions.fetch("frame_ratio")){
			var img_c_width = manager.dimensions.fetch("frame_width");
			var img_c_height = img_c_width / img_ratio;
		} else {
			var img_c_height = manager.dimensions.fetch("frame_height");
			var img_c_width = img_c_height * img_ratio;
		}

		return [{
			index: this.image_index,
			img_xs: 0, img_ys: 0,
			img_width: img_r.width, img_height: img_r.height,
			x: (manager.dimensions.fetch("frame_width") - img_c_width) / 2,
			y: (manager.dimensions.fetch("frame_height") - img_c_height) / 2,
			width: img_c_width,
			height: img_c_height
		}];
	}
}
