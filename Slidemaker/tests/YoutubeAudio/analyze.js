var expect = require('chai').expect;
var path = require("path");
var fs = require("fs");
var rewire = require("rewire");
var sinon = require("sinon");
var YoutubeAudio = rewire("../../YoutubeAudio");
var YoutubeAudioModel = require("../../YoutubeAudio/YoutubeAudio");

describe('YoutubeAudio analyze()', function() {

	before(function(done){
		YoutubeAudio.__set__("YoutubeDownloader", youtubeDownloaderStub);
		YoutubeAudio.__set__("AudioAnalyzer", youtubeAnalyzerStub);
		YoutubeAudio.__set__("YoutubeHouseKeeper", youtubeHouseKeeperStub);
		YoutubeAudio.__set__("SingleImageAnimator", singleImageAnimatorStub);
		done();
	});

	beforeEach(function(){
		sinon.spy(YoutubeAudio.__get__("YoutubeHouseKeeper"), "clean");
	});

	afterEach(function(done){
		YoutubeAudio.__get__("YoutubeHouseKeeper").clean.restore();
		YoutubeAudioModel.remove({}, function(error){
			expect(error).to.be.null;
			done();
		});
	});

	describe('analyze() for new audio request', function () {

		it("Returns the audio_id if requested audio is valid", function(done){
			YoutubeAudio.analyze("down-ok-analyze-ok", function(error, audio_id, audio_uri){
				expect(error).to.be.null;
				expect(audio_id).to.be.an("object");
				expect(audio_uri).to.equal(appUrlRoot + "youtube/down-ok-analyze-ok.mp3");
				done();
			});
		});

		it("Returns the error: invalid if requested audio is invalid", function(done){
			YoutubeAudio.analyze("down-ok-analyze-invalid", function(error, id){
				expect(error).to.be.an("error");
				expect(error.message).to.equal("invalid-file");
				done();
			});
		})
		
		it("Returns 'unknown-error' and triggers YoutubeHouseKeeper cleaning function if anything goes wrong during audio analysis", function(done){
			YoutubeAudio.analyze("down-ok", function(error, id){
				expect(error).to.be.an("error");
				expect(error.message).to.equal("unknown-error");
				expect(id).to.be.oneOf([null, undefined]);
				expect(YoutubeAudio.__get__("YoutubeHouseKeeper").clean.calledOnce).to.be.true;
				expect(YoutubeAudio.__get__("YoutubeHouseKeeper").clean.getCall(0).args[0]).to.equal("down-ok");
				done();
			});
		})

		it("Returns 'unknown-error' and triggers YoutubeHouseKeeper cleaning function if anything goes wrong during video download", function(done){
			YoutubeAudio.analyze("analyze-ok", function(error, id){
				expect(error).to.be.an("error");
				expect(error.message).to.equal("unknown-error");
				expect(id).to.be.oneOf([null, undefined]);
				expect(YoutubeAudio.__get__("YoutubeHouseKeeper").clean.calledOnce).to.be.true;
				expect(YoutubeAudio.__get__("YoutubeHouseKeeper").clean.getCall(0).args[0]).to.equal("analyze-ok");
				done();
			});
		})
	});

	describe('analyze() when requested audio is being analyzed', function () {

		it("Returns the error: busy if requested audio is already being analyzed", function(done){

			YoutubeAudio.analyze("down-ok-analyze-ok-delayed", function(error, id){
			})

			setTimeout(function(){
				YoutubeAudio.analyze("down-ok-analyze-ok-delayed", function(error, id){
					expect(error).to.be.an("error");
					expect(error.message).to.equal("busy");
					expect(id).to.be.oneOf([null, undefined]);
					done();
				})
			}, testConfig.delayIncremet);

		});

	});

	describe('analyze() when requested has already been analyzed', function () {

		it("Returns the audio_id if requested audio has already been analyzed and is valid", function(done){
			YoutubeAudio.analyze("down-ok-analyze-ok", function(){
				YoutubeAudio.analyze("down-ok-analyze-ok", function(error, id, audio_uri){
					expect(error).to.be.null;
					expect(id).not.to.be.null;
					expect(audio_uri).to.equal(appUrlRoot + "youtube/down-ok-analyze-ok.mp3");
					done();
				});
			});
		});

		it("Returns the error: invalid-file if audio has been analyzed and is invalid", function(done){
			YoutubeAudio.analyze("down-ok-analyze-invalid", function(){
				YoutubeAudio.analyze("down-ok-analyze-invalid", function(error, id){
					expect(error).to.be.an("error");
					expect(error.message).to.equal("invalid-file");
					expect(id).to.be.oneOf([undefined, null]);
					done();
				});
			});
		})

	});

});


var youtubeDownloaderStub = {
	download: function(video_code, video_code, callback) {
		if(video_code.indexOf("down-ok") >= 0) {
			callback(null);
		} else {
			callback(new Error("unknown-error"));
		}
	}
};

var singleImageAnimatorStub = function(data){
	this.data = data;
};

singleImageAnimatorStub.prototype.buildAllKeyframeSets = function(){
	if(this.data.energy)
		return [
			[{t:2}],
			[{t:2}, {t:2}],
			[{t:2},{t:2},{t:2},{t:2},{t:2},{t:2},{t:2}] // 7
		]
	else
		return false;
}

var youtubeAnalyzerStub = {
	analyze: function(file, callback){
		setTimeout(function(){
			if(file.indexOf("analyze-ok") >= 0){
				callback(null, '{"energy":[0.24,0.78],"rate_of_energy":[0.1,0.2]}');
			} else if (file.indexOf("analyze-invalid") >= 0){
				callback(new Error("invalid-file"));
			} else {
				callback(new Error("unknown-error"));
			}
		}, file.indexOf("delayed") >= 0 ? (testConfig.delayIncremet * 10) : 0);
	}
};

/*
	Attempts:
		1) remove entry from db where videoCode is <video code>
		2) remove <video code>.* from ./public/youtube/
*/
var youtubeHouseKeeperStub = {
	clean: function(video_code, callback){
		callback(null);
	}
}
