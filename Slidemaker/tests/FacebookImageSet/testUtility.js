var fs = require("fs");
var path = require("path");
var fse = require('fs-extra')
var rewire = require("rewire");
var async = require("async");
var FacebookImageSetModel = require("../../FacebookImageSet/FacebookImageSetModel");

/*
	Removes all database entries for FacebookImageSet collection and 
	facebook user images.
	
	@param callback function(error)
*/
function clearAllFbImageSets(callback){
	FacebookImageSetModel.remove({}, function(error){
		if(error){
			callback(error);
		} else {
			var folder = path.join(
				appRoot, "public",
				"slide_data", "facebook_images");
			fse.emptyDir(folder, function(err){
				if(err){
					callback(true);
				} else {
					callback(null);
				}
			});
		}
	});
}

/*
	Check if files exists.

	@param file string - name of file
	@param callback function(error, exists)
*/
function file_exists(file, callback) {
	fs.stat(file, function(error, stats){
		if(stats && stats.isFile()){
			var exists = true;
		} else {
			var exists = false;
		}
		callback((error && error.code == "ENOENT") ? null : error, exists);
	});
}

/*
	Remove a file if exists

	@param name string - filename
	@param callback function(error)
*/
function remove_file_if_exists(file, callback) {
	file_exists(file, function(error, exists){
		if(error){
			callback(error);
		} else if (exists) {
			fs.unlink(file, callback);
		} else {
			callback(null);
		}
	});
}

module.exports = {
	file_exists: file_exists,
	remove_file_if_exists: remove_file_if_exists,
	clearAllFbImageSets: clearAllFbImageSets
};