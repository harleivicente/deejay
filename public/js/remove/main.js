window.RemoveApp = React.createClass({
	getInitialState: function(){
		return {
			logged_into_facebook: false,
			loading: true,	// first load
			owner: false,	// not the owner
			logging_in: false, // trying to log in
			error: null,	// removal failed
			removing: false, // trying to delete
			ownerId: $("input[name='owner-fb-id']").val()
		}
	},

	componentDidMount: function() {
		$("#progress-bar").html("<div class='progress'><div class='progress-bar progress-bar-success progress-bar-striped active' role='progressbar'></div></div>");
		$(".progress-bar").attr("style", "width:100%");
		var instance = this;
		this.isLoggedFacebook(function(logged){
			instance.setState({
				logged_into_facebook: logged,
				owner: instance.isOwner(),
				loading: false
			});
		});
	},

	/*
		@param callback function(error)
	*/
	remoteRemoveSlide: function(callback){
		var slideId = $("input[name='slide-id']").val();
		var query = "info=" + JSON.stringify({oauth_access_token: FB.getAccessToken()});
		var url = "http://" + window.location.host + "/slides/" + slideId + "?";
		$.ajax({url: url + query, method: "DELETE"})
		.done(function(result){
			callback(!result.status);
		})
		.error(function(){
			callback(true);
		});		
	},

	componentDidUpdate: function(){
		$("#progress-bar").html("<div class='progress'><div class='progress-bar progress-bar-success progress-bar-striped active' role='progressbar'></div></div>");
		$(".progress-bar").attr("style", "width:100%");

	},

	isOwner: function(){
		return FB.getUserID() == this.state.ownerId;
	},

	isLoggedFacebook: function(callback){
	    FB.getLoginStatus(function(response){
	    	if(response.status == "connected"){
	    		callback(true);
	    	} else {
	    		callback(false);
	    	}
	    });	
	},

	clickFacebook: function() {
		var instance = this;
		instance.setState({logging_in: true});
		FB.login(function(response){
			instance.setState({
				logging_in: false,
				logged_into_facebook: response.authResponse,
				owner: instance.isOwner()
			});
		}, {scope:'user_photos'});		
	},

	clickRemoveSlide: function() {
		var instance = this;
		instance.setState({
			removing: true
		});
		instance.remoteRemoveSlide(function(error){
			instance.setState({
				removing: false,
				error: error
			});			
		});
	},

	reloadApp: function(){
		window.location = "/build";
	},

	render: function(){
		var body = "";
		var footer = "";
		
		if(this.state.error === true){
			body = "Something went wrong and we were unable to remove this slideshow. Please, try again in a few moments.";
			footer = <div className="panel-footer">
				<div className="row">
					<div className="col-md-4 col-md-offset-8">
						<button src="/home" onClick={this.reloadApp} className="btn btn-success">Reload app</button>
					</div>
				</div>
			</div>;
		} else if(this.state.error === false){
			body = "Slideshow successfully removed.";
			footer = <div className="panel-footer">
				<div className="row">
					<div className="col-md-4 col-md-offset-8">
						<button src="/home" onClick={this.reloadApp} className="btn btn-success">Home page</button>
					</div>
				</div>
			</div>;
		} else if(this.state.loading){
			body = "Loading slideshow information...";
			footer = <div className="panel-footer">
				<div className="row">
					<div className="col-sm-12">
						<p><strong>Please wait...</strong></p>
						<div id="progress-bar"></div>
					</div>
				</div>
			</div>;
		} else if(!this.state.logged_into_facebook){
			body = <div>
				<p> In order to delete this slide you must log in the facebook account used to create it. </p>
				<div id="ownership-fb">
					<button type="button" className="btn btn-primary" onClick={this.clickFacebook} disabled={this.state.logging_in}><strong>Log into facebook</strong></button>
				</div>
			</div>
		} else if (!this.state.owner){
			body = "You must be the owner in order to remove this slideshow.";
			footer = <div className="panel-footer">
				<div className="row">
					<div className="col-md-4 col-md-offset-8">
						<button src="/home" onClick={this.reloadApp} className="btn btn-success">Reload app</button>
					</div>
				</div>
			</div>;
		} else {
			body = "If you remove this slide all the content associated to it will be permanently deleted. Are you sure you want to continue ?"
			if(this.state.removing){
				footer = <div className="panel-footer">
					<div className="row">
						<div className="col-sm-12">
							<p><strong>Deleting slideshow...</strong></p>
							<div id="progress-bar"></div>
						</div>
					</div>
				</div>;
			} else {
				footer = <div className="panel-footer">
					<div className="row">
						<div className="col-md-4">
							<button onClick={this.reloadApp} className="btn btn-default">Cancel</button>
						</div>				
						<div className="col-md-4 col-md-offset-4">
							<button onClick={this.clickRemoveSlide} className="btn btn-danger">Delete slide</button>
						</div>
					</div>
				</div>;		
			}
		}

		var content = <div className="panel panel-default">
			<div className="panel-heading">
				<h3 className="panel-title">Slideshow configuration.</h3>
			</div>
			<div className="panel-body">
				{body}
			</div>
			{footer}

		</div>

		return <div>
			{content}
		</div>
	}

});

$.getScript("//connect.facebook.net/en_US/sdk.js")
  .done(function( script, textStatus ) {
    FB.init({
      appId      : '1803519839869180',
      cookie     : true, 
      xfbml      : true,
      version    : 'v2.6'
    });
	ReactDOM.render(<RemoveApp />, document.getElementById('react-root-remove'));
  })
.fail(function( jqxhr, settings, exception ) {});	