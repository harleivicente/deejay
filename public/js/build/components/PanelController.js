window.PanelController = React.createClass({
	render: function() {
		var stage = this.props.stage;
		var msg = this.props.main.state.msg;

		if(stage == "choose_audio"){
			return <ChooseAudio main={this.props.main}/>
		} else if (stage == "config_audio") {
			return <ConfigAudio main={this.props.main}/>
		} else if (stage =="choose_images") {
			return <ChooseImages main={this.props.main}/>
		} else if (stage =="build") {
			return <Build main={this.props.main}/>
		} else if (stage == "app_exception") {
			return <AppException main={this.props.main}/>
		} else {
			return <Loader/>
		}
	}
});