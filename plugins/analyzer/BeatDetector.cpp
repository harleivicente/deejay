#include "BeatDetector.h"

#include <cmath>
#include <vector>
#include <stdio.h>
#include <iomanip> 
#include <list>

#define K_ENERGIE_RATIO  1.3 // le ratio entre les energie1024 et energie44100 pour la d�tection des peak d'energie
#define K_TRAIN_DIMP_SIZE 108 // la taille du train d'impulsions pour la convolution (en pack de 1024 (430=10sec))
using namespace std;
using std::list;

BeatDetector::BeatDetector(SoundManager* snd_mgr)
{
    this->snd_mgr = snd_mgr;
    length = snd_mgr->get_length();
    energie1024 = new float[length/1024];
    energie44100 = new float[length/1024];
    conv = new float[length/1024];
    beat = new float[length/1024];
    energie_peak = new float[length/1024+21];
    for(int i=0 ; i<length/1024+21 ; i++) energie_peak[i] = 0;

    rate_of_energy_change = new float[length/1024];
    beats_data = new BeatData[length/1024];

}

BeatDetector::~BeatDetector()
{

}

int BeatDetector::energie(int* data, int offset, int window)
{
    float energie=0.f;
    for(int i=offset ; (i<offset+window)&&(i<length) ; i++)
    {
        energie = energie + data[i]*data[i]/window;
    }
    return (int)energie;
}

void BeatDetector::normalize(float* signal, int size, float max_val)
{
    // recherche de la valeur max du signal
    float max=0.f;
    for(int i=0 ; i<size ; i++)
    {
        if (abs(signal[i])>max) max=abs(signal[i]);
    }
    // ajustage du signal
    float ratio = max_val/max;
    for(int i=0 ; i<size ; i++)
    {
        signal[i] = signal[i]*ratio;
    }
}

int BeatDetector::search_max(float* signal, int pos, int fenetre_half_size)
{
    float max=0.f;
    int max_pos=pos;
    for(int i=pos-fenetre_half_size ; i<=pos+fenetre_half_size ; i++)
    {
        if (signal[i]>max)
        {
            max=signal[i];
            max_pos=i;
        }
    }
    return max_pos;
}

void BeatDetector::audio_process(void)
{
// recupere les donn�es de la musique
// ----------------------------------
    // le canal gauche
    int* data = snd_mgr->get_left_data();

// calcul des energies instantann�es
// ---------------------------------
    for(int i=0 ; i<length/1024 ; i++)
    {
        energie1024[i] = energie(data, 1024*i, 4096); // 4096 pour lisser un peu la courbe
    }
// calcul des energies moyennes sur 1 seconde
// ------------------------------------------
    energie44100[0]=0;
    // la moyenne des 43 premiers energies1024 donne l'energie44100 de la premiere seconde
    float somme=0.f;
    for(int i=0 ; i<43 ; i++)
    {
        somme = somme + energie1024[i];
    }
    energie44100[0]=somme/43;

    // pour toutes les autres, ...
    for(int i=1 ; i<length/1024 ; i++)
    {
        somme = somme - energie1024[i-1] + energie1024[i+42];
        energie44100[i] = somme/43;
    }

// Ratio energie1024/energie44100
// ------------------------------
    for(int i=21 ; i<length/1024 ; i++)
    {
        // -21 pour centrer le energie1024 sur la seconde du energie44100
        if (energie1024[i]>K_ENERGIE_RATIO*energie44100[i-21])
        {
            energie_peak[i]=1;
        }
    }

// Calcul des BPMs
// ---------------
    // calcul des laps de temps entre chaque energie_peak
    vector<int> T;
    int i_prec=0;
    for(int i=1 ; i<length/1024 ; i++)
    {
        if((energie_peak[i]==1)&&(energie_peak[i-1]==0))
        {
            int di = i-i_prec;
            if (di>5) // rien pour les parasites
            {
                T.push_back(di);
                i_prec=i;
            }
        }
    }
    // le tableau T contient tous les laps de temps
    // fait des stats pour savoir quel est le plus fr�quent
    int T_occ_max=0;
    float T_occ_moy=0.f;

        // compte les occurence de chaque laps de temps
    int occurences_T[86]; // max 2 paquets de 43 d'�cart (2sec)
    for(int i=0 ; i<86 ; i++) occurences_T[i] = 0;
    for(int i=1 ; i<T.size() ; i++)
    {
        if(T[i]<=86) occurences_T[T[i]]++;
    }
    int occ_max=0;
    for(int i=1 ; i<86 ; i++)
    {
        if (occurences_T[i]>occ_max)
        {
            T_occ_max=i;
            occ_max = occurences_T[i];
        }
    }
        // on fait la moyenne du max + son max de voisin pour + de pr�cision
    int voisin = T_occ_max-1;
    if (occurences_T[T_occ_max+1]>occurences_T[voisin]) voisin = T_occ_max+1;
    float div = occurences_T[T_occ_max] + occurences_T[voisin];

    if (div==0) T_occ_moy = 0;
    else T_occ_moy = (float)(T_occ_max*occurences_T[T_occ_max] + (voisin)*occurences_T[voisin]) / div;

    // clacul du tempo en BPMs
    tempo = (int)60.f/(T_occ_moy*(1024.f/44100.f));

// Calcul de la Beat line
// ----------------------
    // cr�ation d'un train d'impulsions (doit valoir 1 tous les T_occ_moy et 0 ailleurs)
    float train_dimp[K_TRAIN_DIMP_SIZE];
    float espace=0.f;
    train_dimp[0]=1.f;
    for(int i=1 ; i<K_TRAIN_DIMP_SIZE ; i++)
    {
        if (espace>=T_occ_moy)
        {
            train_dimp[i]=1;
            espace=espace-T_occ_moy; // on garde le depassement
        }
        else train_dimp[i]=0;
        espace+=1.f;
    }

    // convolution avec l'�nergir instantann�e de la music
    for(int i=0 ; i<length/1024-K_TRAIN_DIMP_SIZE ; i++)
    {
        for(int j=0 ; j<K_TRAIN_DIMP_SIZE ; j++)
        {
            conv[i] = conv[i] + energie1024[i+j] * train_dimp[j];
        }

    }
    normalize(conv, length/1024, 1.f);

    // recherche des peak de la conv
        // le max (c'est la plupart du temps un beat (pas tout le temps ...))
    for(int i=1 ; i<length/1024 ; i++) 
        beat[i]=0;

    float max_conv=0.f;
    int max_conv_pos=0;
    for(int i=1 ; i<length/1024 ; i++)
    {
        if(conv[i]>max_conv)
        {
            max_conv=conv[i];
            max_conv_pos=i;
        }
    }
    beat[max_conv_pos]=1.f;

        // les suivants
            // vers la droite
    int i=max_conv_pos+T_occ_max;
    while((i<length/1024)&&(conv[i]>0.f))
    {
        // on cherche un max dans les parages
        int conv_max_pos_loc = search_max(conv, i, 2);
        beat[conv_max_pos_loc]=1.f;

        i=conv_max_pos_loc+T_occ_max;
    }
            // vers la gauche
    i=max_conv_pos-T_occ_max;
    while(i>0)
    {
        // on cherche un max dans les parages
        int conv_max_pos_loc = search_max(conv, i, 2);
        beat[conv_max_pos_loc]=1.f;

        i=conv_max_pos_loc-T_occ_max;
    }

    /*
        Prepare data
    */
    
    // Normalize energie44100 energy array        
    normalize_float_array(energie44100, length/1024);
    // Create rate of change array, not normalized
    create_rate_of_change_float_array(energie44100, rate_of_energy_change, length/1024);
    // Normalize rate of change array
    normalize_float_array(rate_of_energy_change, length/1024);
    // Populate beats_data array with data already normalized
    create_beat_data_lists();

    output_data();
}

float* BeatDetector::get_energie1024(void)
{
    return energie1024;
}

float* BeatDetector::get_energie44100(void)
{
    return energie44100;
}

float* BeatDetector::get_energie_peak(void)
{
    return energie_peak;
}

float* BeatDetector::get_conv()
{
    return conv;
}

float* BeatDetector::get_beat()
{
    return beat;
}

int BeatDetector::get_tempo(void)
{
    return tempo;
}

/*
    Sends beat song analysis data through standard output
    @see main.cpp
*/
void BeatDetector::output_data(void){
    bool first_beat = true;
    cout << "{";

    // Energy
    cout << "\"energy\":[";
    for(int i = 0; i < length/1024; i++){
        if(i > 0){
            cout << ",";        
        }
        cout << fixed << setprecision(3) << energie44100[i];  
    }
    cout << "],";

    // Rate of energy
    cout << "\"rate_of_energy\":[";
    for(int i = 0; i < length/1024; i++){
        if(i > 0){
            cout << ",";        
        }
        cout << fixed << setprecision(3) << rate_of_energy_change[i];  
    }
    cout << "],";

    // Beat times
    cout << "\"beats\":[";
    bool first = true;
    for(int i = 0; i < length/1024; i++){
        if(beats_data[i].valid){
            if(!first){
                cout << ",";        
            } else {
                first = false;
            }
            cout << beats_data[i].time;  
        }
    }
    cout << "],";

    // Beat intensity
    first = true;
    cout << "\"beats_intensity\":[";
    for(int i = 0; i < length/1024; i++){
        if(beats_data[i].valid){
            if(!first){
                cout << ",";        
            } else {
                first = false;
            }
            cout << fixed << setprecision(3) << beats_data[i].intensity;  
        }
    }
    cout << "],";

    // Beat intensity rate of change
    first = true;
    cout << "\"beats_intensity_rate\":[";
    for(int i = 0; i < length/1024; i++){
        if(beats_data[i].valid){
            if(!first){
                cout << ",";        
            } else {
                first = false;
            }
            cout << fixed << setprecision(3) << beats_data[i].intensity_rate;  
        }
    }
    cout << "]}";
}

void BeatDetector::normalize_float_array(float* array, int size) {
    float max = 0.f;

    for(int i = 0; i < size; i++) {
        if(abs(array[i]) > max){
            max = abs(array[i]);
        }
    }

    for(int i = 0; i < size; i++) {
        array[i] = (array[i] / max);
    }    
}

void BeatDetector::create_rate_of_change_float_array(float* array, float* target, int size){
    /*
        1 second has
        44100/1024 ~= 43 samples

        Therefore two samples are separated by
        1/43 ~= 0.0233 seconds or 23 ms
    */
    float deltaTime = 23.3; // milliseconds

    for(int i = 0; i < size; i++) {
        if(i == (size - 1)){
            target[i] = 0;
        } else {
            target[i] = (array[i+1] - array[i]) / deltaTime;
        } 
    }  
}

void BeatDetector::create_beat_data_lists() {
    float max_beat_intensity = 0.f;
    float max_beat_rate_of_change = 0.f;
    int index_last_valid_beat = -1.f;

    for(int i = 0; i < length/1024; i++){
        bool is_first_beat = (index_last_valid_beat < 0);
        float current_beat = beat[i]; // greater than zero if beat is present
        int current_beat_time = (int)((i + 1) * 23.22); // ms
        float current_beat_int = energie1024[i];

        beats_data[i].time = current_beat_time;
        beats_data[i].intensity = current_beat_int;
        beats_data[i].valid = true;
        
        if(current_beat > 0 && !is_first_beat) {
            float prev_beat_int = energie1024[index_last_valid_beat];
            int prev_beat_time = (int)((index_last_valid_beat + 1) * 23.22); //ms
            beats_data[i].intensity_rate = (current_beat_int-prev_beat_int)/(current_beat_time-prev_beat_time);
            index_last_valid_beat = i;
        } else if(current_beat > 0 && is_first_beat) {
            index_last_valid_beat = i;
            beats_data[i].intensity_rate = (beats_data[0].intensity/beats_data[0].time);
        } else {
            beats_data[i].valid = false;
        }

        // looking for max beat intensity/rate of change
        if(beats_data[i].valid){
            if(abs(current_beat_int) > max_beat_intensity){
                max_beat_intensity = abs(current_beat_int);
            }
            if(abs(beats_data[i].intensity_rate) > max_beat_rate_of_change){
                max_beat_rate_of_change = abs(beats_data[i].intensity_rate);
            }
        }
    }

    // Normalize beat intensity/rate of change
    for(int i = 0; i < length/1024; i++){
        beats_data[i].intensity = (beats_data[i].intensity / max_beat_intensity);
        beats_data[i].intensity_rate = (beats_data[i].intensity_rate / max_beat_rate_of_change);
    }
}

