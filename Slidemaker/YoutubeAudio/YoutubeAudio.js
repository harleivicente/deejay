var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var path = require("path");

var model = new Schema({
	pending: {type: Boolean, default: true, required: true},
	invalid: {type: Boolean, default: null, required: false},
	videoCode: {type: String, required: true},
	analysisData: {type: String},
	keyframeSets: {type: String},
	duration: {type: Number} // Soundtrack duration in ms
});

/*
	Insert new audio for analysis.

	@param video_code string
	@param callback function(error, audio_id)

*/
model.statics.insert = function(video_code, callback){
	var new_item = new Audio({
		pending: true,
		invalid: null,
		videoCode: video_code
	});
	new_item.save(function(error, item){
		if(error){
			callback(error, null);
		} else {
			callback(error, item._id);
		}
	});
}

/*
	The audio is invalid. Should only be called
	if audio is STATUS.PENDING.

	@param audio_id string
	@param callback function(error)
*/
model.statics.invalidate = function (audio_id, callback){
	Audio.findById(audio_id, function(error, item){
		if(error || !item || !item.pending){
			callback(new Error("unknown-error"));
		} else {
			item.pending = false;
			item.invalid = true;
			item.save(function(error){
				if(error){
					callback(new Error("unknown-error"));
				} else {
					callback(null);					
				}
			});
		}
	})	
}

/*
	The analysis has been successful. Should only be 
	called if audio is STATUS.PENDING.
	Analysis data/keyframeSets must pe provided.
	
	@param audio_id string
	@param analysis_data string
	@param keyframe_sets array of keyframeSet @info SingleImageAnimator
	@param callback function(error)
*/
model.statics.pass = function (audio_id, analysis_data, keyframe_sets, callback){
	Audio.findById(audio_id, function(error, item){
		if(error || !item || !item.pending){
			callback(new Error("unknown-error"));
		} else {
			// Audio duration in ms
			var parsed = JSON.parse(analysis_data);
			var ms = Math.round((parsed.energy.length*1000*1024)/44100);
			item.pending = false;
			item.invalid = false;
			item.duration = ms;
			item.analysisData = analysis_data;
			item.keyframeSets = JSON.stringify(keyframe_sets);
			item.save(callback);
		}
	})
}

/*
	@param video_code string
	@param callback function(error, Audio)
*/
model.statics.getAudioByVideoCode = function (video_code, callback) {
	Audio.findOne({videoCode: video_code}, callback);
}


/*
	@param audio_id string
	@param callback function(error, Audio)
*/
model.statics.getAudio = function (audio_id, callback) {
	Audio.findById(audio_id, callback);
}

/*
	Obtain keyframeSet

	@param index number
	@return mixed keyframeSet | false 
*/
model.methods.getKeyframeSet = function(index){
	var sets = JSON.parse(this.keyframeSets);
	if(sets || sets[index]){
		return sets[index];
	} else {
		return false;
	}
}

/*
	Get uri to audio file
*/
model.methods.getUri = function(){
	return appUrlRoot + "youtube/" + this.videoCode + ".mp3";
}

var Audio = mongoose.model('Audio', model);
module.exports = Audio;


