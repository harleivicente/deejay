window.ChooseImages = React.createClass({
	getInitialState: function() {
		return {
			loading: false
		}
	},	
	render: function() {
		var main = this.props.main;

		if(this.state.loading){
			var footer = <div className="row">
				<div className="col-sm-12">
					<p><strong>Loading facebook data... </strong></p>
					<div id="progress-bar"></div>
				</div>
			</div>;
		} else {
			var footer = <div className="row">
				<div className="col-sm-4">
						<button type="button" onClick={this.handlePrevious} className="btn btn-default">Back</button>
					</div>
				</div>
		}

		return  <div className="panel panel-default">
			<div className="panel-heading">
				<h3 className="panel-title">Choose images</h3>
			</div>
			<div className="panel-body">
				
				<p> Please select between 24 and 250 images. Your images will be stretched over the duration of the audio you chose. So, the more images the faster the slideshow. Currenlty we only support loading images from your facebook account. </p>

				<div id="img-src-icons" className="row">
					<button type="button" className="btn btn-primary" onClick={this.clickFacebook} disabled={this.state.loading}><strong>Log into facebook</strong></button>
				</div>
				
			</div>
			<div className="panel-footer">
				{footer}
			</div>			
		</div>
	},
	handlePrevious: function(){
		this.props.main.setState({stage: "config_audio"});
	},
	isLogged: function(callback){
	    FB.getLoginStatus(function(response){
	    	if(response.status == "connected"){
	    		callback(true);
	    	} else {
	    		callback(false);
	    	}
	    });	
	},
	clickFacebook: function(e) {
		var instance = this;

		var photo_selector_ready_handler = function() {
			instance.props.main.setState({
				stage: "choose_images"
			});
		}

		this.isLogged(function(logged){
			if(logged){
				instance.setState({loading: true});
				instance.csphoto_start(null, photo_selector_ready_handler);
			} else {
				FB.login(function(response){
					if(response.authResponse){
						instance.setState({loading: true});
						instance.csphoto_start(null, photo_selector_ready_handler);
					}
				}, {scope:'user_photos'});
			}
		});
	},
	imagesSelected: function(imageIds) {
		this.props.main.setState({
				image_ids: imageIds,
				stage: "build"
			}
		);
	},
	imageSelectionCanceled: function(){
		this.setState({loading: false});
	},
	componentDidUpdate: function() {
		$("#progress-bar").html("<div class='progress'><div class='progress-bar progress-bar-success progress-bar-striped active' role='progressbar'></div></div>");
		$(".progress-bar").attr("style", "width:100%");
	},	
	csphoto_start: function(id, callback) {
	    if (!id) id = 'me';
	    var main = this.props.main;


	    var callbackAlbumSelected = function(albumId) {
	      var album, name;
	      album = CSPhotoSelector.getAlbumById(albumId);
	      csphoto.showPhotoSelector(null, album.id);
	    };

	    // Initialise the Photo Selector with options that will apply to all instances
	    CSPhotoSelector.init({debug: false});

	    // Create Photo Selector instances
	    var csphoto = CSPhotoSelector.newInstance({
	      callbackAlbumSelected : callbackAlbumSelected,
	      callbackSubmit: 	this.imagesSelected,
	      callbackSelectionCanceled: this.imageSelectionCanceled,
	      minSelection      : main.state.nof_images_min,
	      maxSelection      : main.state.nof_images_max,
	      albumsPerPage     : 6,
	      photosPerPage     : 500,
	      autoDeselection     : false
	    });

	    // reset and show album selector
	    csphoto.reset();
	    csphoto.showAlbumSelector(id, callback);

	    //make photo selector panel visible
	    $("#csphoto-panel").removeClass("hide");
	}
});
