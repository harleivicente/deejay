var path = require("path");
var async = require("async");
var fs = require("fs");
var YoutubeAudioModel = require(path.join(appRoot, "Slidemaker", "YoutubeAudio", "YoutubeAudio"));

/*
	Attemps to clear de system of any
	data associated to a video:

	1) entry from db
	2) ./public/youtube/<videoCode>.mp3
	3) ./public/youtube/<videoCode>.mp4

	@throws Error if anything goes wrong.
*/
function clean(video_code, callback){
	var youtube_folder_path = path.join(appRoot, "public", "youtube");

	async.parallel([
		
		// Remove db entry
		function(cb){
			YoutubeAudioModel.remove({videoCode: video_code}, cb);
		},

		// Attempt to remove mp3 file
		function(cb){
			fs.stat(path.join(youtube_folder_path, video_code + ".mp3"), function(error, stats){
				if(error){
					cb(null);
				} else {
			fs.unlink(path.join(youtube_folder_path, video_code + ".mp3"), cb);
				}
			})
		},

		// Attempt to remove mp4 file
		function(cb){
			fs.stat(path.join(youtube_folder_path, video_code + ".mp4"), function(error, stats){
				if(error){
					cb(null);
				} else {
			fs.unlink(path.join(youtube_folder_path, video_code + ".mp4"), cb);
				}
			})
		}

	], function(error){
		if(error){
			throw new Error("Unable to complete youtube clean up function.");
		} 
		callback(error);
	});

}

module.exports.clean = clean;