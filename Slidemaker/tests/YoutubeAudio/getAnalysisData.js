var expect = require('chai').expect;
var path = require("path");
var fs = require("fs");
var rewire = require("rewire");
var YoutubeAudio = rewire("../../YoutubeAudio");
var YoutubeAudioModel = require("../../YoutubeAudio/YoutubeAudio");

describe('YoutubeAudio getAnalysisData()', function() {

	before(function(done){
		YoutubeAudio.__set__("YoutubeDownloader", youtubeDownloaderStub);
		YoutubeAudio.__set__("SingleImageAnimator", singleImageAnimatorStub);
		YoutubeAudio.__set__("AudioAnalyzer", youtubeAnalyzerStub);
		done();
	});

	afterEach(function(done){
		YoutubeAudioModel.remove({}, function(error){
			expect(error).to.be.null;
			done();
		});
	});

	it("Returns invalid-file if requested audio does not exist", function(done){
		YoutubeAudio.getAnalysisData("down-ok-analyze-ok-madeupcode", function(error, data){
			expect(error).to.be.an("error");
			expect(error.message).to.equal("invalid-file");
			expect(data).to.be.null;
			done();
		})
	});

	it("Returns the analysis data if requested audio is valid", function(done){
		YoutubeAudio.analyze("down-ok-analyze-ok", function(error, id){
			YoutubeAudio.getAnalysisData(id, function(error, data){
				expect(error).to.be.null;
				expect(data).to.equal('{"energy":[0.24,0.78],"rate_of_energy":[0.1,0.2]}');
				done();
			})
		})
	})
});


var youtubeDownloaderStub = {
	download: function(video_code, video_code, callback) {
		if(video_code.indexOf("down-ok") >= 0) {
			callback(null);
		} else {
			callback(new Error("unknown-error"));
		}
	}
};

var youtubeAnalyzerStub = {
	analyze: function(file, callback){
		setTimeout(function(){
			if(file.indexOf("analyze-ok") >= 0){
				callback(null, '{"energy":[0.24,0.78],"rate_of_energy":[0.1,0.2]}');
			} else if (file.indexOf("analyze-invalid") >= 0){
				callback(new Error("invalid-file"));
			} else {
				callback(new Error("unknown-error"));
			}
		}, file.indexOf("delayed") >= 0 ? (testConfig.delayIncremet * 10) : 0);
	}
};

var singleImageAnimatorStub = function(data){
	this.data = data;
};

singleImageAnimatorStub.prototype.buildAllKeyframeSets = function(){
	if(this.data.energy)
		return [
			[{t:2}],
			[{t:2}, {t:2}],
			[{t:2},{t:2},{t:2},{t:2},{t:2},{t:2},{t:2}] // 7
		]
	else
		return false;
}
