var mongoose = require("mongoose");
var YoutubeModel = require("../Slidemaker/YoutubeAudio/YoutubeAudio");
var SlideshowModel = require("../Slidemaker/SlideshowModel");
var YoutubeAudioModel = require("../Slidemaker/YoutubeAudio/YoutubeAudio");

mongoose.connect('mongodb://localhost/db');
var db = mongoose.connection;

db.on('error', function(){
	console.log('Failed to connect to MongoDB.');
	process.exit();
});

db.once('open', function() {
	var slideId = process.argv[2];
	SlideshowModel.findById(slideId, function(error, item){
		console.log("Slideshow object:")
		console.log("=========>");
		console.log(item);
		console.log("=========>");
	});

});

