var FacebookImageSetModel = require("./FacebookImageSetModel");
var fbgraph = require("fbgraph");
var async = require("async");
var download = require('download-file');
var path = require("path");
var util = require("util");
var mongoose = require("mongoose");
var fs = require("fs");

fbgraph.setOptions({
    timeout:  20000,
  	pool:     { maxSockets:  Infinity },
	headers:  { connection:  "keep-alive" }
});

/*
	Insert facebook image set

	@param options object {
		facebookUserId: <>,
		images: [<id>, ...]
	}
	@param callback function(Error, id)
		Errors: 
			null
			unknown-error
*/
function create(options, callback){
	console.log("creating image set.");
	if(!options.facebookUserId || !util.isArray(options.images)){
		callback(new Error("unknown-error"), null);
	} else {
		var newImageSetId = mongoose.Types.ObjectId();

		// Obtain uri of images and download
		async.mapLimit(
			options.images,
			20,
			function(imageId, cb){

				var params = {
					id: imageId,
					access_token: fbAppId + "|" + fbAppSecret
				};

				fbgraph.get("/?fields=images", params, 
					function(err, res) {
						console.log(err);
						/*
							Some images wont download from facebook. Skip them 
							and remove them from 'options.images'.
						*/
						if(err && err.code == 100){
							var key = options.images.indexOf(imageId);
							options.images.slice(key, 1);
							cb(false);
						} else if(err || !res.images || res.images.length == 0 || res.id != imageId){
							cb(true);
						} else {
							// Download
							var params = {
								directory: path.join(appRoot, "public", "slide_data", "facebook_images"),
								filename: newImageSetId + imageId
							}
							download(res.images[0].source, params, function(err){
								cb(err);
							}) 
						}
				});					

			}, 
			function(error){
				if(error){
					callback(new Error("unknown-error"));
				} else {
					FacebookImageSetModel.insert(
						options.facebookUserId,
						options.images,
						newImageSetId,
						callback
					);
				}
			}
		);
	}
}

/*
	@param imageSetId string
	@param callback function(Error)
		error types:
			null
			unknown-error
*/
function remove(imageSetId, callback){
	FacebookImageSetModel.findById(imageSetId, function(error, item){
		if(error || !item) {
			callback(new Error("unknown-error"));
		} else {

			// Remove images
			async.map(item.images,
				function(imageId, callback){
					var file = path.join(
						appRoot, "public",
						"slide_data", "facebook_images",
						imageSetId + imageId);
					remove_file_if_exists(file, function(error){
						callback(error);
					});
				},
				function(error){
					if(error){
						callback(new Error("unknown-error"));
					} else {
						item.remove().then(function(){
							callback(null);
						}).error(callback);
					}
				}
			);	
		}
	});
}

/*
	Verifies ownership of image set through
	image set configuration.
	
	@param imageSetId string
	@param data object 
		{
			oauth_access_token: <string>
		}
	@param callback function(error, boolean)
		Errors:
			null
			unknown-error
*/
function verifyOwnership(imageSetId, data, callback){
	// Fetch imageSet
	FacebookImageSetModel.findById(imageSetId, function(error, item){
		if(error || !item){
			callback(new Error("unknown-error"), null);
		} else {
			var params = {
				input_token: data.oauth_access_token,
				access_token: fbAppId + "|" + fbAppSecret
			};

			fbgraph.get("debug_token", params,
				function(err, res) {
					if(err || !res.data.is_valid){
						callback(new Error("unknown-error"), null);
					} else if(res.data.user_id == item.facebookUserId){
						callback(null, true);
					} else {
						callback(new Error("unknown-error"), null);
					}
			});
		}
	});
}

/*

	Obtains image uris.

	@param imageSetId string
	@param callback function(Error, imageUris)

	Erros:
		null
		unknown-error

	imageUris: [<string>, ...]
*/
function getUris(imageSetId, callback){
	FacebookImageSetModel.getUris(imageSetId, callback);
}

module.exports = {
	create: create,
	remove: remove,
	verifyOwnership: verifyOwnership,
	getUris: getUris
};


/*
	Check if files exists.

	@param file string - name of file
	@param callback function(error, exists)
*/
function file_exists(file, callback) {
	fs.stat(file, function(error, stats){
		if(stats && stats.isFile()){
			var exists = true;
		} else {
			var exists = false;
		}
		callback((error && error.code == "ENOENT") ? null : error, exists);
	});
}

/*
	Remove a file if exists

	@param name string - filename
	@param callback function(error)
*/
function remove_file_if_exists(file, callback) {
	file_exists(file, function(error, exists){
		if(error){
			callback(error);
		} else if (exists) {
			fs.unlink(file, callback);
		} else {
			callback(null);
		}
	});
}
