var expect = require('chai').expect;
var path = require("path");
var fs = require("fs");
var async = require("async");
var YoutubeAudioModel = require("../../YoutubeAudio/YoutubeAudio");
var YoutubeHouseKeeper = require("../../YoutubeAudio/YoutubeHouseKeeper");

describe('YoutubeHouseKeeper clean()', function() {

	beforeEach(function(done){
		async.parallel([
			function(cb){
				YoutubeAudioModel.remove({}, cb);
			},
			function(cb) {
				remove_file_if_exists("testfile.mp3", cb);
			}, 
			function(cb) {
				remove_file_if_exists("testfile.mp4", cb);
			}
		], function(error){
			expect(error).to.be.null;
			done();
		});
	});

	it("Does not fail when no files need to be removed.", function(done){
		YoutubeHouseKeeper.clean("makeupcode", function(error){
			expect(error).to.be.null;
			done();
		})
	});

	it("Does not fail when database entry, .mp3 and .mp4 need to be removed.", function(done){

		async.parallel([
			// create fake mp3 file
			function(cb){
				create_empty_file("testfile.mp3", cb);
			},
			// create fake mp4 file
			function(cb){
				create_empty_file("testfile.mp4", cb);
			},
			// insert fake database entry
			function(cb){
				YoutubeAudioModel.insert("testfile", cb);
			}
		], function(error, results){
			expect(error).to.be.null;

			// attempt to clean
			YoutubeHouseKeeper.clean("testfile", function(error){
				expect(error).to.be.null;
				async.parallel({
					mp3Exists: function(cb){
						file_exists("testfile.mp3", cb);
					},
					mp4Exists: function(cb){
						file_exists("testfile.mp4", cb);
					},
					dbEntryExists: function(cb){
						YoutubeAudioModel.find({videoCode: "testfile"}, function(error, items){
							cb(error, (items.length > 0));
						})
					}
				}, function(error, results){
					expect(error).to.be.null;
					expect(results.mp3Exists).to.be.false;
					expect(results.mp4Exists).to.be.false;
					expect(results.dbEntryExists).to.be.false;
					done();
				});
			})			

		});
	});
});

/*
	Creates placeholder file in folder
	./public/youtube

	@param name string - filename
	@param callback function(error)
*/
function create_empty_file(name, callback) {
	fs.writeFile(path.join(appRoot, "public", "youtube", name), 'placeholder', function(error){
		callback(error);
	});
}

/*
	Check if files in folder 
	./public/youtube exists.

	@param name string - name of file
	@param callback function(error, exists)
*/
function file_exists(name, callback) {
	fs.stat(path.join(appRoot, "public", "youtube", name), function(error, stats){
		if(stats && stats.isFile()){
			var exists = true;
		} else {
			var exists = false;
		}
		callback(error.code == "ENOENT" ? null : error, exists);
	});
}

/*
	Remove a file if exists from folder
	./public/youtube

	@param name string - filename
	@param callback function(error)
*/
function remove_file_if_exists(name, callback) {
	file_exists(name, function(error, exists){
		if(error){
			callback(error);
		} else if (exists) {
			fs.unlink(path.join(appRoot, "public", "youtube", name), callback);
		} else {
			callback(null);
		}
	});
}