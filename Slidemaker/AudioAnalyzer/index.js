var fs = require("fs");
var path = require("path");
var execFile = require("child_process").execFile;

/*
	Analyzes file.
*/

/*
	Attempts to analyze a file
	
	@param file string
	@parm function callback(Error, analysis_data)
	 	analysis_data string | null

		Errors:
			> file-missing
			> invalid-file - Analyzer is unable to analyze
			> unknown-error
			> null

	@info for output format @see ./plugins/analyzer/main.cpp
*/
function analyze_song(file, callback) {
	file_exists(file, function(error, exists){
		if(error || !exists){
			callback(new Error("file-missing"), null);
		} else {
			run_plugin(file, function(error, output){
				if(error){
					callback(new Error("unknown-error"), null);
				} else if(output.valid) {
					callback(null, output.data);
				} else {
					callback(new Error("invalid-file"), null);
				}
			});
		}
	});
}

/*
	@param file string
	@param callback function(Error, output)
		
	@todo check semantics of data returned by plugin.

	output: {
		valid: boolean // if plugin thinks its valid
		data: data | null
	}

	Errors:
		null
		unknown-error
*/
function run_plugin(file, callback){
	var command_file = path.join(appRoot, "plugins", "analyzer", "main");
	var args_list = [
		file
	];
	execFile(command_file, args_list, function(error, stdout, stderr){
		try {
			var valid_json = true;
			var json = JSON.parse(stdout);
		} catch(e) {
			valid_json = false;
		}

		/*
			Currently only doing base checks
			on the integrity of analysis data.
		*/
		if(
			error ||
			!valid_json || 
			json.energy.length == 0 ||
			json.rate_of_energy.length == 0 ||
			json.beats.length == 0 ||
			json.beats_intensity.length == 0) 
		{
			callback(null, {valid: false, data: null});
		} else {
			callback(null, {valid: true, data: stdout});
		}
	});
}

/*
	Check if file exists.

	@param file string
	@param callback function(error, exists)
*/
function file_exists(name, callback) {
	fs.stat(name, function(error, stats){
		if(!error && stats && stats.isFile()){
			callback(null, true);
		} else {
			callback(null, false);
		}
	});
}

module.exports.analyze = analyze_song;