var Youtube = require("./YoutubeAudio");
var SlideshowModel = require("./SlideshowModel");
var moduleUtil = require("./ModuleUtil");
var ModuleUtil = require("./ModuleUtil");
var async = require("async");
require("../config");
var FacebookImageSet = require('./FacebookImageSet');
var FacebookImageSetModel = require('./FacebookImageSet/FacebookImageSetModel');

/*
	Slidemaker

*/


/*
	Analyzes beat pattern of audio.

	@param source_type string
		Indicates how the server will acquire
		audio to be analyzed. Valid options:
		"youtube"

	@param options object
		Aditional options particular to each
		source type. The format for each
		source type:
			> youtube
				{
					video_code: <string> youtube 
					url video code.
				}

	@param callback function(Error, audio_id, audio_uri)
		Erros:
			null
			invalid-file
			unknown-error
			busy

		audio_id: <string | null>
*/
function analyze(source_type, options, callback){
	if(source_type == "youtube"){
		Youtube.analyze(options.video_code, callback);
	} else {
		callback(new Error("unknown-error"));
	}
}

/*
	Obtains the min/max number of images needed for a given Audio.

	@param audio_source_type string ["youtube"]
	@param audio_source_id string
	@param timeStart time - time in ms
	@param timeEnd time - time in ms
	@param callback function(error, min, max)
		Types of errors:
			- unknown-error
*/
function getNumberOfImageLimits(audio_source_type, audio_source_id, timeStart, timeEnd, callback){
	var moduleName = ModuleUtil.getModuleName(audio_source_type, 'audio');
	var AudioModule = require("./"+moduleName);
	async.waterfall([
		// Check validity of audio source
		function(cb){
			var validAudio = validAudioSource.indexOf(audio_source_type) >= 0;
			cb(!validAudio);
		},
		// Get min/max number of images
		function(audio, cb){
			AudioModule.getNumberOfImageLimits(
				audio_source_id, timeStart, timeEnd, callback);
		}
	]);	
}

/*
	Save slideshow to server.

	@param data object {
			audio_source, [youtube]
			audio_id
			start_time <seconds> (opt)
			end_time <seconds> (opt)
			image_set_source [facebook]
			image_set_config: aditional data for
				the given image source.

					> facebook:
						imageIds: <array of fb ids>
						facebookUserId: <>
		}
	
	@param callback function(error, slide_id)
*/
function createSlideshow(data, callback){
	async.waterfall([
		// Check validity of audio/video sources
		function(cb){
			var validImg = validImageSources.indexOf(data.image_set_source) >= 0;
			var validAudio = validAudioSource.indexOf(data.audio_source) >= 0;
			cb(!validImg || !validAudio);
		},
		// Check the audio is ready to be used
		function(cb){
			var moduleName = moduleUtil.getModuleName(data.audio_source, "audio");
			var AudioModule = require("./" + moduleName);
			AudioModule.isReady(data.audio_id, function(error, ready){
				cb(error || !ready);
			});
		},
		// Create image set
		function(cb) {
			if(data.image_set_source == "facebook"){
				var FacebookImageSet = require("./FacebookImageSet");
				FacebookImageSet.create(
					{
						facebookUserId: data.image_set_config.facebookUserId,
						images: data.image_set_config.imageIds
					},
					cb
				)
			} else {
				cb(true);
			}
		},
		// Create slideshow instance
		function(imageSetId, cb){
			delete data.image_set_config;
			data.image_set_id = imageSetId;
			SlideshowModel.insert(data, cb);
		}
	], callback);
}

/*
	Gets uri of audio file.

	@param slide_id string

	@param callback function(Error, uri)
		errors
			null
			unknown-error

		uri: <string | null>
*/
function getAudioUri(slide_id, callback){
	SlideshowModel.findById(slide_id, function(error, slide){
		if(error || !slide){
			callback(new Error("unknown-error"));
		} else if (slide.audioSource == "youtube") {
			Youtube.getUri(slide.audioSourceId, function(error, uri){
				if(error){
					callback(new Error("unknown-error"));
				} else {
					callback(null, uri);
				}
			});
		} else {
			callback(new Error("unknown-error"));
		}
	});
}

/*
	Obtains the keyframe set for the slideshow.

	@param slide_id string
	@param callback function(Error, keyframeSet)
		Error
			null - no problems encountered
			unknown-error - something went wrong.

		keyframeSet @see SingleImageAnimator for more info

*/
function getKeyframeSet(slide_id, callback) {
	var AudioModule = require("./YoutubeAudio/YoutubeAudio");

	async.waterfall([
		// Load slide
		function(cb){
			SlideshowModel.findById(slide_id, function(error, slide){
				cb(error||!slide, slide);
			});
		},
		// Load audio
		function(slide, cb){
			AudioModule.findById(slide.audioSourceId, function(error, audio){
				cb(error||!audio, audio, slide.keyframeSetIndex);
			});
		},
		// Load 
		function(audio, keyframeSetIndex, cb){
			var set = audio.getKeyframeSet(keyframeSetIndex); 
			if(set === false){
				cb(true, null);
			} else {
				cb(null, set);
			}
		}
	], function(error, set){
		if(error){
			callback(new Error("unknown-error"), null);
		} else {
			callback(null, set);
		}
	});
}

/*
	Obtains list of image uris that are to be
	used in slideshow.

	@parm slide_id string
	@param callback function(error, uris)
		errors:
			null
			unknown-error
		uris - [<uri>, ...]
*/
function getImageUris(slide_id, callback){
	SlideshowModel.findById(slide_id, function(error, slide){
		if(error || !slide){
			callback(new Error("unknown-error"));
		} else if (slide.imageSource == "facebook") {
			FacebookImageSet.getUris(slide.imageSourceId, function(error, data){
				if(error){
					callback(new Error("unknown-error"));
				} else {
					callback(null, data);
				}
			});
		} else {
			callback(new Error("unknown-error"));
		}
	});
}


/*
	Allows creator of slideshow to remove it.
	
	@param slideId string
	@param ownershipProof object - information
		used identify ownership of slideshow.
		Depends on sources of imageSet
		used to created slideshow.

			> facebook: {
				oauth_access_token: <oauth
				access token given to app
				by user through facebook>
			}
		}

	@param callback function(Error, bool)
		Errors:
			null
			unknown-error

*/
function removeSlideshow(slideId, ownershipProof, callback) {
	SlideshowModel.findById(slideId, function(error, slide){
		if(error || !slide){
			callback(new Error("unknown-error"));
		} else if (slide.audioSource == "youtube" && slide.imageSource == "facebook") {
			FacebookImageSet.verifyOwnership(slide.imageSourceId, ownershipProof, function(err, owner){
				if(err || !owner){
					callback(new Error("unknown-error"));
				} else {
					SlideshowModel.removeComplete(slideId, callback);
				}
			});
		} else {
			callback(new Error("unknown-error"));
		}
	});
}

module.exports = {
	removeSlideshow: removeSlideshow,
	createSlideshow: createSlideshow,
	getImageUris: getImageUris,
	getNumberOfImageLimits: getNumberOfImageLimits,
	analyze: analyze,
	getAudioUri: getAudioUri,
	getKeyframeSet: getKeyframeSet
}