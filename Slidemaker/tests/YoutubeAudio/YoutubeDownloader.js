var expect = require('chai').expect;
var path = require("path");
var fs = require("fs");
var YoutubeDownloader = require("../../YoutubeAudio/YoutubeDownloader");

describe('YoutubeDownloader', function() {

	before(function(done){
		remove_downloaded_youtube_file("testaudiofile", function(error){
			expect(error).to.be.null;
			done();
		});
	});

	describe('download()', function () {

		it("it returns 'unknown-error' when given fake youtube video code", function (done) {
			YoutubeDownloader.download("fakecode", "basename", function(error){
				expect(error).to.be.an("error");
				expect(error.message).to.equal("unknown-error");
				done();
			})
		});

		it("it returns no errors when valid youtube video code is given", function(done){
			var video_code = testConfig.validYoutubeVideoCode;
			YoutubeDownloader.download(video_code, "testaudiofile", function(error){
				expect(error).to.be.null;
				
				// Check if file was downloaded and remove
				remove_downloaded_youtube_file("testaudiofile", function(error, file_present){
					expect(error).to.be.null;
					expect(file_present).to.be.true;
					done();
				});
			});
		})

	});

});

/*
	Attempts to remove file ./public/youtube/<name>.mp3

	@param name string
	@param callback function(error, file_present)

*/
function remove_downloaded_youtube_file(name, callback){
	var file_exists = true;
	var filename = path.join(appRoot, "public", "youtube", name + ".mp3");
	fs.stat(filename, function(error, stat){
		if(error || !stat.isFile()){
			callback(null, false);
		} else {
			fs.unlink(filename, function(error){
				callback(error, true);
			});
		}
	});
}
