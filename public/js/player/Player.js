/*
	@prop dev boolean
*/
window.Player = React.createClass({
	render: function(){
		var classes = this.props.dev ? "hide debug-mode" : "hide";

		return <div id="playback" className={classes}>
			<div id="canvas_holder">
				<canvas id="canvas"></canvas>
			</div>
			<div id="player-controls">
				<span className="command glyphicon glyphicon-pause pause hide"></span>
				<span className="command glyphicon glyphicon-play play"></span>
				<span className="command glyphicon glyphicon-repeat repeat"></span>
				<span className="menu glyphicon glyphicon-home new"></span>
				<span className="menu glyphicon glyphicon-share share"></span>
			</div>
		</div>
	}
});