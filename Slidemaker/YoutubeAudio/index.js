/*

	Module: Youtube audio processing module.

*/

var YoutubeAudio = require("./YoutubeAudio");
var YoutubeDownloader = require("./YoutubeDownloader");
var AudioAnalyzer = require("../AudioAnalyzer");
var YoutubeHouseKeeper = require("./YoutubeHouseKeeper");
var SingleImageAnimator = require("../SingleImageAnimator");
var async = require("async");
var path = require("path");

/*
	Analyzes beat pattern of youtube video.
	
	If audio requested is not already being processed.

	1 - Download audio into public/youtube folder as mp3.
	2 - Analyze audio.
	3 - Generate keyframesets
	4 - Update database.
	5 - Return audio_id if successful.

	@param callback function(Error, audio_id, audio_uri)
		Errors:
			null - no problems encountered
			"invalid-file" - the analyzer is imcom-
				patible with the choosen audio.
			"unknown-error" - something went wrong.
			"busy" - audio is already being processed.
		audio_id: <string | null>
		audio_uri - uri to mp3 file
*/
function analyze(video_code, callback){
	YoutubeAudio.getAudioByVideoCode(video_code, function(error, audio){
		if(error) {
			callback(new Error("unknown-error"));
		} else if(!audio) {
				async.waterfall([

					// Insert audio in db
					function(cb){
						YoutubeAudio.insert(video_code, function(error, id){
							cb(error, id);
						});

					// Download audio
					}, function(audio_id, cb){
						YoutubeDownloader.download(video_code, video_code, function(error){
							cb(error, audio_id);
						});

					// Analyze audio
					}, function(audio_id, cb){
						var filename = path.join(appRoot, "public", "youtube", video_code + ".mp3");
						AudioAnalyzer.analyze(
							filename, function(error, analysis_data_json){
								cb(error, audio_id, analysis_data_json);
							}
						);

					// Build keyframeSets & Update audio status
					}, function(audio_id, analysis_data_json, cb){

						// Generate keyframeSets
						var analysis_data = JSON.parse(analysis_data_json);
						var animator = new SingleImageAnimator(analysis_data);
						var keyframeSets = animator.buildAllKeyframeSets();

						if(keyframeSets === false){
							cb(new Error("unknown-error"));
						} else {
							YoutubeAudio.pass(audio_id, analysis_data_json, keyframeSets,
								function(error){
									cb(error, audio_id);
							});
						}

					}

				], function(error, audio_id){
					if(error && error.message == "invalid-file"){
						YoutubeAudio.invalidate(audio_id, function(err){
							if(err){
								YoutubeHouseKeeper.clean(video_code, function(){
									callback(new Error("unknown-error"));
								});
							} else {
								callback(error);
							}
						});
					} else if(error){
						if(audio_id){
							YoutubeHouseKeeper.clean(video_code, function(){
								callback(error);
							});
						} else {
							callback(error);
						}
					} else {
						callback(
							null,
							audio_id,
							appUrlRoot + "youtube/" + video_code + ".mp3"
						);
					}
				});
			} else if(audio.pending){
				callback(new Error("busy"));
			} else if(audio.invalid) {
				callback(new Error("invalid-file"));
			} else {
				callback(
					null,
					audio.id,
					appUrlRoot + "youtube/" + video_code + ".mp3"
				);
			}
	});
}

/*
	Gets uri to audio file.

	@param audio_id string

	@param callback function(error, uri)
		error: <string | null>
				null - no problems encountered
				"invalid" - the requested audio
				does not exists.
				<other type of error>
		uri: <string | null>
*/
function getUri(audio_id, callback){
	YoutubeAudio.getAudio(audio_id, function(error, audio){
		if (error || !audio) {
			callback(new Error("invalid-file"), null);
		} else {
			callback(null, audio.getUri());
		}
	});
}

/*
	Gets beat analysis data for audio.

	@param audio_id string

	@param callback function(error, analysisData)
		error: <string | null>
				null - no problems encountered
				"invalid-file" - the requested audio
				does not exists.
				<other type of error>
		analysisData: <string | null>
*/
function getAnalysisData(audio_id, callback){
	YoutubeAudio.getAudio(audio_id, function(error, audio){
		if (error || !audio){
			callback(new Error("invalid-file"), null);
		} else {
			callback(null, audio.analysisData);
		}
	});
}

/*
	Get the minimum/maximum number of images needed
	to use an youtube audio in a slide.

	@param audio_id string
	@param timeStart number - time in ms
	@param timeEnd number - time in ms
	@param callback function(error, min, max)
		error Error 
		min number
		max number

		Error types:
			- unknown-error

	If YoutubeAudio has no keyframeSets will return zero.
	If a keyframe set has not keyframes zero images are need for it.
*/
function getNumberOfImageLimits(audio_id, timeStart, timeEnd, callback){
	YoutubeAudio.getAudio(audio_id, function(error, audio){
		var failTimeFormat = typeof timeStart != "number" || typeof timeEnd != "number";
		var failMinDuratiton = Math.abs(timeEnd-timeStart) < global.minSlideDuration;
		
		if (error || !audio || failMinDuratiton || failTimeFormat){
			callback(new Error("unknown-error"), null, null);
		} else {
			/*
				Attempt to correct invalid inputs
			*/
			if(timeStart > timeEnd) {
				var temp = timeStart;
				timeStart = timeEnd;
				timeEnd = temp;
			}
			if(timeStart < 1){
				timeStart = 1;
			}
			if(timeEnd > audio.duration){
				timeEnd = audio.duration;
			}

			// Check if keyframes exist
			var fail = false;
			try{
				var sets = JSON.parse(audio.keyframeSets);
			} catch(e) {
				fail = true;
			}
			if(fail || sets.length < 1){
				callback(null, 0, 0);
			} else {

				// Check for min
				var min = _countKeyframes(sets[0], timeStart, timeEnd);

				// Check for max
				var max = _countKeyframes(sets[sets.length-1], timeStart, timeEnd);

				if(!min || !max){
					callback(new Error("unknown-error"), null, null);
				} else {
					callback(null, min, max);
				}
			}
		}
	});	
}


/*
	Chooses the best keyframe set for a given 
	audio selection and number of images.

	@param audio_id string
	@param timeStart number - ms
	@param timeEnd number - ms
	@param nofImages number
	@param callback function(error, index)

		Types of errors:
			- unknown-error

	If YoutubeAudio has no keyframeSets will return zero.
	If a keyframe set has not keyframes zero images are need for it.			
*/
function getBestKeyframeSetIndex(audio_id, timeStart, timeEnd, nofImages, callback){
	YoutubeAudio.getAudio(audio_id, function(error, audio){

		// Attempt to parse number from input
		timeStart = Number.parseInt(timeStart);
		timeEnd = Number.parseInt(timeEnd);

		var failTimeFormat = typeof timeStart != "number" || typeof timeEnd != "number";
		var failMinDuratiton = Math.abs(timeEnd-timeStart) < global.minSlideDuration;
		var failNofImages = typeof nofImages != "number" || nofImages < 0;

		if (error || !audio || failMinDuratiton || failTimeFormat || failNofImages){
			callback(new Error("unknown-error"), null);
		} else {
			/*
				Attempt to correct invalid inputs
			*/
			if(timeStart > timeEnd) {
				var temp = timeStart;
				timeStart = timeEnd;
				timeEnd = temp;
			}
			if(timeStart < 1){
				timeStart = 1;
			}
			if(timeEnd > audio.duration){
				timeEnd = audio.duration;
			}			

			// Check if keyframes exist
			var fail = false;
			try{
				var sets = JSON.parse(audio.keyframeSets);
			} catch(e) {
				fail = true;
			}
			if(fail || sets.length < 1){
				callback(null, 0);
			} else {
				var clearedIndex = 0;
				for(var i=0 ; i<sets.length; i++){
					var count = _countKeyframes(sets[i], timeStart, timeEnd);
					if(count === false){
						callback(new Error("unknown-error"), null);
					} else if(nofImages >= count){
						clearedIndex = i;
					} else {
						break;
					}
				}
				callback(null, clearedIndex);
			}
		}
	});
}


/*
	Checks if youtube audio is ready to be used.

	@param audio_id string
	@param callback function(error, bool)

*/
function isReady(audio_id, callback){
	YoutubeAudio.getAudio(audio_id, function(error, audio){
		if(error || !audio){
			callback(null, false);
		} else {
			callback(null, !audio.pending && !audio.invalid);
		}
	});
}

/*
	Counts number of keyframes within the indicated range.

	@param keyframeSet - array of {t: <time in ms>, i: <0-1>}
	@param timeStart number
	@param timeEnd number
	@return mixed number | false
*/
function _countKeyframes(keyframeSet, timeStart, timeEnd) {
	if(
		typeof timeStart != "number" ||
		typeof timeEnd != "number" ||
		timeStart > timeEnd ||
		!Array.isArray(keyframeSet)
	){
		return false;
	} else {
		var count = 0;
		for(var i=0; i<keyframeSet.length; i++){
			var current = keyframeSet[i].t;
			// have not reached range
			if(current < timeStart){
				continue;
			// within range
			} else if(current >= timeStart && current <= timeEnd){
				count++;
				continue;
			// past range
			} else {
				break;
			}
		}
		return count;
	}
}

module.exports = {
	getUri: getUri,
	isReady: isReady,
	analyze: analyze,
	getAnalysisData: getAnalysisData,
	getBestKeyframeSetIndex: getBestKeyframeSetIndex,
	getNumberOfImageLimits: getNumberOfImageLimits
}