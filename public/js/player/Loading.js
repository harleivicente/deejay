window.Loading = React.createClass({
	render: function(){
		var checkpoints_html = [];
		var main = this.props.main;

		for (var key in main.state.checkpoints){
			var current = main.state.checkpoints[key];

			if(current.ready === false){
				checkpoints_html.push(<div className="loading-checklist">
					<img className="loading" src="/images/loading.gif"></img><strong>{current.text}</strong>
				</div>);
			} else if(current.ready === true){
				checkpoints_html.push(<div className="loading-checklist">
					<span className="glyphicon glyphicon-ok" aria-hidden="true"></span> {current.text}
				</div>);
			} else {
				checkpoints_html.push(<div className="loading-checklist">
					<span className="glyphicon glyphicon-time" aria-hidden="true"></span> {current.text}
				</div>);
			}
		}

		return <div className="panel panel-default">
			<div className="panel-heading">
				<h3 className="panel-title">Loading slide</h3>
			</div>
			<div className="panel-body">
				{checkpoints_html}
			</div>
		</div>
	}

});