var expect = require('chai').expect;
var path = require("path");
var fs = require("fs");
var rewire = require("rewire");
var YoutubeAudio = rewire("../../YoutubeAudio");
var YoutubeAudioModel = require("../../YoutubeAudio/YoutubeAudio");
var async = require("async");
var saveConfigMinSlideDuration = null;

describe('YoutubeAudio getBestKeyframeSetIndex()', function() {

	before(function(done){
		YoutubeAudio.__set__("YoutubeDownloader", youtubeDownloaderStub);
		YoutubeAudio.__set__("AudioAnalyzer", youtubeAnalyzerStub);
		YoutubeAudio.__set__("SingleImageAnimator", singleImageAnimatorStub);
		saveConfigMinSlideDuration = global.minSlideDuration;
		global.minSlideDuration = 5; // ms
		done();
	});

	after(function(){
		global.minSlideDuration = saveConfigMinSlideDuration;
	});

	beforeEach(function(done){
		YoutubeAudioModel.remove({}, function(error){
			expect(error).to.be.null;
			done();
		});
	});

	afterEach(function(done){
		YoutubeAudioModel.remove({}, function(error){
			expect(error).to.be.null;
			done();
		});
	});

	it("Returns an error for audio that does not exist", function(done){
		YoutubeAudio.getBestKeyframeSetIndex("fakeid", 2, 46, 23, function(error, index){
			expect(error).to.be.an("error");
			expect(index).to.be.null;
			done();
		})
	});

	it("Corrects input if requesting data from time out of boundaries", function(done){
		async.waterfall([
			function(cb){YoutubeAudio.analyze("down-ok-analyze-ok", cb);},
			function(audio_id, audio_uri, cb){

				/*
					Test a few different inputs
				*/
				async.parallel([
					function(pCb){
						YoutubeAudio.getBestKeyframeSetIndex(audio_id, 4, 47, 12, function(error, index){
							expect(error).to.be.null;
							expect(index).to.equal(2);
							pCb(null);
						});
					},
					function(pCb){
						YoutubeAudio.getBestKeyframeSetIndex(audio_id, -2, 18, 8, function(error, index){
							expect(error).to.be.null;
							expect(index).to.equal(1);
							pCb(null);
						});
					},
					function(pCb){
						YoutubeAudio.getBestKeyframeSetIndex(audio_id, 10, 4, 3, function(error, index){
							expect(error).to.be.null;
							expect(index).to.equal(1);
							pCb(null);
						});
					}	
				], function(e, r){
					done();
				});

			}
		]);
	});

	it("Returns an error if number of images is not valid value", function(done){
		async.waterfall([
			function(cb){YoutubeAudio.analyze("down-ok-analyze-ok", cb);},
			function(audio_id, audio_uri, cb){

				/*
					Test a few different inputs
				*/
				async.parallel([
					function(pCb){
						YoutubeAudio.getBestKeyframeSetIndex(audio_id, 4, 40, "text", function(error, index){
							expect(error).to.be.an("error"); 
							expect(index).to.be.null;
							pCb(null);
						});
					},
					function(pCb){
						YoutubeAudio.getBestKeyframeSetIndex(audio_id, 1, 12, -12, function(error, index){
							expect(error).to.be.an("error");
							expect(index).to.be.null;
							pCb(null);
						});
					}
				], function(e, r){
					done();
				});
			}
		]);
	});

	it("Returns correct answer for each sample input", function(done){
		async.waterfall([
			function(cb){YoutubeAudio.analyze("down-ok-analyze-ok", cb);},
			function(audio_id, audio_uri, cb){
				/*
					Test a few different inputs
				*/
				async.parallel([
					function(pCb){
						YoutubeAudio.getBestKeyframeSetIndex(audio_id, 1, 20, 3, function(error, index){
							expect(error).to.be.null;
							expect(index).to.equal(0);
							pCb(null);
						});
					},
					function(pCb){
						YoutubeAudio.getBestKeyframeSetIndex(audio_id, 1, 20, 8, function(error, index){
							expect(error).to.be.null;
							expect(index).to.be.equal(1);
							pCb(null);
						});
					},
					function(pCb){
						YoutubeAudio.getBestKeyframeSetIndex(audio_id, 8, 15, 3, function(error, index){
							expect(error).to.be.null;
							expect(index).to.be.equal(1);
							pCb(null);
						});
					},
					function(pCb){
						YoutubeAudio.getBestKeyframeSetIndex(audio_id, 8, 15, 5, function(error, index){
							expect(error).to.be.null;
							expect(index).to.be.equal(2);
							pCb(null);
						});
					}
				], function(e, r){
					done();
				});
			}
		]);
	});	
});


var youtubeDownloaderStub = {
	download: function(video_code, video_code, callback) {
		if(video_code.indexOf("down-ok") >= 0) {
			callback(null);
		} else {
			callback(new Error("unknown-error"));
		}
	}
};

var youtubeAnalyzerStub = {
	analyze: function(file, callback){
		setTimeout(function(){
			if(file.indexOf("analyze-ok") >= 0){
				callback(null, 
					'{"energy":[0.24,0.78],"rate_of_energy":[0.1,0.2]}'
				);
			} else if (file.indexOf("analyze-invalid") >= 0){
				callback(new Error("invalid-file"));
			} else {
				callback(new Error("unknown-error"));
			}
		}, file.indexOf("delayed") >= 0 ? (testConfig.delayIncremet * 10) : 0);
	}
};

var singleImageAnimatorStub = function(data){
	this.data = data;
};

singleImageAnimatorStub.prototype.buildAllKeyframeSets = function(){
	if(this.data.energy)

		/*
			Fake data.
			3 keyframe sets of 46 ms audio.
		*/
		return [
			[
				{t:1, i: 0.5}, {t:8, i: 0.5}, {t:9, i: 0.5}, {t:17, i: 0.5}
			],
			[	
				{t:1, i:0.5}, {t:6, i:0.5},{t:8, i: 0.5},{t:11, i: 0.5},
				{t:14, i: 0.5},{t:17, i: 0.5},{t:19, i: 0.5}
			],
			[	
				{t:2, i:0.5}, {t:5, i:0.5},{t:6, i: 0.5},{t:7, i: 0.5},
				{t:10, i:0.5}, {t:11, i:0.5},{t:13, i: 0.5},{t:15, i: 0.5},
				{t:17, i: 0.5},{t:19, i: 0.5}
			]
		]
	else
		return false;
}


