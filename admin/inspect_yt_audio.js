var mongoose = require("mongoose");
var YoutubeModel = require("../Slidemaker/YoutubeAudio/YoutubeAudio");
var SlideshowModel = require("../Slidemaker/SlideshowModel");
var YoutubeAudioModel = require("../Slidemaker/YoutubeAudio/YoutubeAudio");

mongoose.connect('mongodb://localhost/db');
var db = mongoose.connection;

db.on('error', function(){
	console.log('Failed to connect to MongoDB.');
	process.exit();
});

db.once('open', function() {
	var videoCode = process.argv[2];
	YoutubeAudioModel.find({videoCode: videoCode}, function(error, data){
		var audio = data[0];
		console.log("Video Code: ", audio.videoCode);
		console.log("Pending: ", audio.pending);
		console.log("Invalid: ", audio.invalid);
		console.log("Duration: ", audio.duration);
		console.log("ID: ", audio._id);


		console.log("\n--------KEYFRAME SETS-----------")
		var keyframeSets = JSON.parse(audio.keyframeSets);
		console.log("Number of keyframe sets: " + keyframeSets.length);
		for(var i = 0; i < keyframeSets.length; i++){
			console.log("	Set #" + i + " needs " + keyframeSets[i].length + " image(s)")
		}
		console.log("---------------------------------")
		

		console.log("\n-------  IDENTIFIED BEATS  ------")
		var data = JSON.parse(audio.analysisData);
		var avg = data.beats.length / (audio.duration/1000);
		avg *= 60;
		avg = avg.toFixed(2);
		console.log("Avg beats per minute: ", avg);
		console.log("---------------------------------")
		process.exit();
	});

});

