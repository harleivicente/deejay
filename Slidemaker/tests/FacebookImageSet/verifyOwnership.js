var expect = require('chai').expect;
var FacebookImageSet = require("../../FacebookImageSet");
var FacebookImageSetModel = require("../../FacebookImageSet/FacebookImageSetModel");
var testUtility = require("./testUtility");

describe('FacebookImageSet verifyOwnership()', function() {

	beforeEach(function(done){
		testUtility.clearAllFbImageSets(function(error){
			expect(error).to.be.null;
			done();
		})
	})

	afterEach(function(done){
		testUtility.clearAllFbImageSets(function(error){
			expect(error).to.be.null;
			done();
		})
	})

	it("Returns unknown-error if invalid image set if given", function(done){
		FacebookImageSet.verifyOwnership("makeupidhere", {oauth_access_token: testConfig.fbUserId}, function(error, valid){
			expect(error).to.be.an("error");
			expect(error.message).to.equal("unknown-error");
			expect(valid).to.be.null;
			done();
		});
	})

	it("Returns true if code given is valid", function(done){
		var options = {
			facebookUserId: testConfig.fbUserId,
			images: testConfig.fbImages
		};
		FacebookImageSet.create(options, function(error, id){
			expect(error).to.be.null;
			var token = testConfig.oauthAccessToken;

			FacebookImageSet.verifyOwnership(id, {oauth_access_token: token}, function(error, valid){
				expect(error, "Make sure to configure facebook user and access token.").to.be.null;
				expect(valid, "Make sure to configure facebook user and access token.").to.be.true;
				done();
			});
		});
	})

	it("Returns unknown-error if code given is not valid", function(done){
		var options = {
			facebookUserId: testConfig.fbUserId,
			images: testConfig.fbImages
		};		
		FacebookImageSet.create(options, function(error, id){
			expect(error).to.be.null;

			var fakeToken = "EAAZAoSq3mmP3BAI3zvQKhpTvuluXhW8Hvx8j74xWyHIHJ4Qgo7PeWDsAVQ7vDxxjwlImmfFgZC1fo48U2zhWQroAN6snj40khZAmVgEOEi789jVZCRPaZCubJMSqoZANDQnJBSOwUKJVrLckJ7ZC9Nm9OrFlzj2AoLeSNz8P0xrJAZDZD";
			FacebookImageSet.verifyOwnership(id, {oauth_access_token: fakeToken}, function(error, valid){
				expect(error).to.be.an("error");
				expect(error.message).to.be.equal("unknown-error");
				expect(valid).to.be.null;
				done();
			});
		});		
	})

});

