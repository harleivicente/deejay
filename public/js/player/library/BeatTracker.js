/*
	Animating beat indicator. Updates debug information.
*/
function BeatTracker(manager) {
	this.manager = manager;
	this.dev_mode = manager.dev_mode;

	/*
		How far from previous beat
		to move onto next one (ms).
	*/
	this.trigger_threshold = 165;

	/*
		Beat animation

		Indicator has base radius.
		Radius will be multipled by 
		X over the animation.

		       *
			  * *
			 *   *
			*     *
		* *         * * 
			   ^
			   B
			   e
			   a
			   t

	*/
	this.animation_duration = 150;
	this.scale_factor = 1.8;
	this.base_radius = 16; // px

	/*
		Used in debug mode.
		Threshold to consider beat radius growth as valid
		beat representation (0 to 1). I.E 0.65 means
		that when at 65% of max radius (base * scale factor)
		it is considered that indicator displayed a beat.
	*/
	this.detection_threshold = 0.8;
	this.detection_threshold_abs = this.base_radius * this.scale_factor * this.detection_threshold;

	/*
		-----------------
		PRIVATE VARIABLES
		-----------------
	*/

	/*
		Beat indicator radius
	*/
	this.indicator_radius = null;

	/* 
		Index of next beat. Points
		to previous range if within a 
		threshold `trigger_threshold`
		or to the next one.
	*/
	this.beat_index = null;

	/*
		Points to next beat that needs
		to be displayed by the beat indicator.
	*/
	this.displayed_beat_index = null;

	/*
		Attributes used to track beats shown
		by indicator		
	*/
	this.max_beat_identifier_error = null;
	this.sample_beat_identifier_errors = null;
	this.displayed_beats = null;

	this.reset();
}

/*
	Prepares to restart animation
	from soundtrack initial trim point.
*/
BeatTracker.prototype.reset = function(){
	this.indicator_radius = this.base_radius;
	this.beat_index = this.manager.beatSet.initial_beat_index;
	this.displayed_beat_index = this.manager.beatSet.initial_beat_index;
	this.max_beat_identifier_error = 0;
	this.displayed_beats = 0;
	this.sample_beat_identifier_errors = [];	
}

/*
	Main loop function for beat tracker
*/
BeatTracker.prototype.update = function() {
	manager.beatTracker.update_beat_index();
	manager.beatTracker.update_indicator_radius();
}

/*
	Update beat indicator radius. In debug mode, also
	identifer errors if timming of beat display.
*/
BeatTracker.prototype.update_indicator_radius = function(){

	/*
		Calculate radius
	*/
	this.indicator_radius = this._calculateIndicatorRadius();

	/*
		DEBUG: track displayed beats.
	*/
	if(this.dev_mode){
		var fell_behind = this.displayed_beat_index < this.beat_index;

		/*
			If beat indicator fell behind, catch it back up
		*/
		if(fell_behind){
			this.displayed_beat_index = this.beat_index;
		}

		/*
			Count a beat once
		*/
		var fresh_beat = this.displayed_beat_index == this.beat_index;
		if(fresh_beat){
			/*
				Once within threshold check how
				far from current beat (this.diplayed_beat_index)
			*/
			var beat_displayed = this.indicator_radius >= this.detection_threshold_abs;
			if(beat_displayed){
				/*
					Log time difference between time when beat indicator
					displayed beat and when it should have.
				*/
				var error = Math.abs(
					this.manager.beatSet.getBeatTime(this.displayed_beat_index) -
					this.manager.time()
				);

				// Add another error sample
				this.sample_beat_identifier_errors.push(error);
				var new_state = {
					displayed_indicator_beats: this.displayed_beats,
					avg_beat_identifier_error: this._avgArray(this.sample_beat_identifier_errors)
				};
				// Update max error if needed
				if(
					this.max_beat_identifier_error === null ||
					error > this.max_beat_identifier_error
				) {
					this.max_beat_identifier_error = error;
					new_state.max_beat_identifier_error = error;
				}
				this.manager.playerRoot.state.debugger.setState(new_state);	
				
				this.displayed_beat_index++;
				this.displayed_beats++;
			}
		}

	}
}

/*
	Index will point to next beat. Once previous is far away 
	enough, index will be updated.

*/
BeatTracker.prototype.update_beat_index = function() {
	var currentBeatTime = manager.beatSet.getBeatTime(this.beat_index);
	var now = manager.time();

	var deltaTime = currentBeatTime - now;
	var futureBeat = deltaTime > 0;

	/*
		If far away enough in the past go to next
	*/
	if (
		!futureBeat &&
		Math.abs(deltaTime) >= this.trigger_threshold &&
		manager.beatSet.times[this.beat_index + 1]
	) {
		this.beat_index++;
	}
}

/*
	Interpolates in fade in fashion

	@param x value from 0 to 1
	@return number from 0 to 1
*/
BeatTracker.prototype._fadeIn = function(x){
	if(x < 0) {
		return 0;
	} else if(x > 1) {
		return 1;
	} else {
		return Math.pow(x, 2);
	}
}		

/*
	Calculates average of array of numbers

	@array array
	@return mixed number | false
*/
BeatTracker.prototype._avgArray = function(array){
	if(!array.length){
		return false;
	} else {
		var total = 0;
		for(var i = 0; i < array.length; i++){
			total += array[i];
		}
		return total/array.length;
	}
}

/*
	Calculates the radius for the beat indicator
*/
BeatTracker.prototype._calculateIndicatorRadius = function(){
	var timeUntilNextBeat = Math.abs(
		this.manager.beatSet.getBeatTime(this.beat_index) - manager.time());	
	var halfDuration = this.animation_duration / 2;

	if(timeUntilNextBeat > halfDuration){
		var radius = this.base_radius;
	} else {
		var x = (halfDuration - timeUntilNextBeat)/halfDuration;
		var y = this._fadeIn(x);
		var radius = this.base_radius*(1 + y*(this.scale_factor-1));
	}		
	return radius;
}
