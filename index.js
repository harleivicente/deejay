require("./config");
var path = require('path');
var http = require('http');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var Slidemaker = require("./Slidemaker");
var virtualhost = require('virtualhost');

var bodyParser = require('body-parser');
var express = require('express');
var app = express();
var session = require('express-session');
var async = require('async');
var FacebookImageSet = require('./Slidemaker/FacebookImageSet');
var FacebookImageSetModel = require('./Slidemaker/FacebookImageSet/FacebookImageSetModel');
var SlideshowModel = require('./Slidemaker/SlideshowModel');
var YoutubeAudioModel = require("./Slidemaker/YoutubeAudio/YoutubeAudio");
var debug_mode = process.env.NODE_ENV === "development";

app.set('view engine', 'pug');
var appViewsFolder = path.join(appRoot, "views");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
  secret: 'keyboard cat secret',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: null }
}))
app.use(express.static(path.join(appRoot, "public")));

/*
	Call handlers
*/

	// Build
	app.get("/build", function(req, res){
		res.render(path.join(appViewsFolder, "pages/build"), {});
	});

	// Privacy
	app.get("/privacy", function(req, res){
		res.render(path.join(appViewsFolder, "pages/privacy"), {});
	});

	// Get settings page
	app.get("/slides/:id/remove", function(req, res){
		async.waterfall([
			function(cb){
				SlideshowModel.findById(req.params.id, function(error, slide){
					if(error || !slide){
						cb(true, null);
					} else {
						cb(false, slide.imageSourceId);
					}
				});
			},
			function(image_set_id, cb){
				FacebookImageSetModel.findById(image_set_id, function(error, imageSet){
					if(error || !imageSet){
						cb(true, null);
					} else {
						cb(null, imageSet.facebookUserId);
					}
				});
			}
			], function(error, owner_fb_id){
			if(error){
				res.redirect(appUrlRoot);
			} else {
				res.render(path.join(appViewsFolder, "pages/remove"), {owner_fb_id: owner_fb_id.toString(), slide_id: req.params.id});
			}
		});
	});	

	/*
		@query info object: Information used to identify
		ownership of slideshow. Depends on
		source of images:

			> facebook:
				{
					oauth_access_token: <user`s token>
				}

		@return object {
			status: boolean
			code: string
		}

		codes: 
			unknown
	*/
	app.delete("/slides/:id", function(req, res){
		var info = null;
		try{
			info = JSON.parse(req.query.info);
		} catch(e){
			info = {};	
		}

		Slidemaker.removeSlideshow(req.params.id, info, function(error){
			if(error){
				res.json({status: false, code: "unknown"});
			} else {
				res.json({status: true, code: null});
			}
		})
	});

	// Play
	app.get("/slides/:id/play", function(req, res){
		async.parallel({
			audioUri: function(cb){
				Slidemaker.getAudioUri(req.params.id, cb);
			},
			keyframeSet: function(cb){
				Slidemaker.getKeyframeSet(req.params.id, cb);
			},
			imageUris: function(cb){
				Slidemaker.getImageUris(req.params.id, cb);
			},
			soundtrackTrim: function(cb){
				SlideshowModel.findById(req.params.id, function(error, slide){
					if(error || !slide){
						cb(true);
					} else {
						cb(null, slide.trim);
					}
				});
			},
			/*
				If in dev mode collect data about beat times and intensities
			*/
			beats: function(cb){
				if(!debug_mode){
					cb(null, null);
				} else {
					async.waterfall([
						// Fetch slideshow object
						function(innerCb){
							SlideshowModel.findById(req.params.id, function(error, slide){
								if(error || !slide){
									innerCb(true);
								} else {
									innerCb(null, slide);
								}
							});
						},
						// Fetch youtube audio object
						function(slide, innerCb){
							YoutubeAudioModel.findById(slide.audioSourceId, function(error, audio){
								if(error || !audio){
									innerCb(true);
								} else {
									innerCb(null, audio);
								}							
							})
						},
						// Parse json and fetch beat data
						function(audio, innerCb){
							var parsed = JSON.parse(audio.analysisData);
							cb(null, {times: parsed.beats, intensities: parsed.beats_intensity});
						}
					]);
				}
			}			
		}, function(error, results){
			if(!error){
				var view_args = {
					dev: (process.env.NODE_ENV === "development"),
					trim_start: results.soundtrackTrim.start,
					trim_end: results.soundtrackTrim.end,
					audio_uri: results.audioUri,
					keyframe_set: results.keyframeSet,
					image_uris: results.imageUris,
					slide_uri: appUrlRoot + req.originalUrl.substring(1),
					slide_remove_page: appUrlRoot + "slides/" + req.params.id + "/remove",
					slide_removed: 0,
					exception: 0
				};

				if(debug_mode){
					view_args.beats = results.beats 
				}

				res.render(path.join(appViewsFolder, "pages/play"), view_args);
			} else {
				/*
					Verify if slideshow exists
				*/
				SlideshowModel.findById(req.params.id, function(error, slide){
					console.log(error);
					if(slide || error){
						res.render(path.join(appViewsFolder, "pages/play"), {
							slide_removed: 0,
							exception: 1
						});
					} else {
						res.render(path.join(appViewsFolder, "pages/play"), {
							slide_removed: 1,
							exception: 0
						});
					}
				});
			}
		});
	});

	/*
		Handles audio analyze requests

		@params	
			audio_source_type - youtube
			config - aditional configuration for 
			each audio source type:
				youtube: {
					video_code - video url code
				}	

		@return object {
			status: boolean,
			code: string,
			audio_id: string
		}

		codes: 
			invalid - audio cannot be processed
			unknown

	*/
	app.post("/audios", function(req, res){
		Slidemaker.analyze(req.body.audio_source_type, req.body.config, function(error, audio_id, audio_uri){
			if(error && error.message == "invalid-file"){
				res.json({status: false, code: "invalid"});
			} else if (error) {
				res.json({status: false, code: "unknown"});
			} else {
				res.json({status: true, audio_id: audio_id, audio_uri: audio_uri});
			}
		});
	});

	/*
		Obtains min/max number of needed images for
		given soundtrack.
		
		@param audio_source_type ["youtube"]
		@param audio_source_id 
		@param time_start - time in ms
		@param time_end - time in ms

		@return object {
			status: boolean,
			code: string,
			min: number,
			max: number
		}

		codes: 
			unknown		
	*/
	app.get("/neededImages", function(req, res){
		var start = Number.parseInt(req.query.time_start);
		var end = Number.parseInt(req.query.time_end);
		Slidemaker.getNumberOfImageLimits(
			"youtube", req.query.audio_source_id,
			start, end, function(error, min, max){
				if(error){
					res.json({status: false, code: "unknown"});
				} else {
					res.json({status: true, code: null, min: min, max: max});
				}
		});
	})

	/*
	POST /slideshows
		
		Save slideshow to server.

		@params 
			audio_source [youtube]
			audio_id
			start_time <seconds>
			end_time <seconds>
			image_set_source [facebook]
			image_set_config:
				> facebook
					{
						imageIds: <array of fb ids>
						facebookUserId: <>
					}

		Return
			{status: bool, slide_id: <id>, code: <>}
				code: unknown | null

	*/
	app.post("/slideshows", function(req, res){
		Slidemaker.createSlideshow(req.body, function(error, slideId){
			if(error){
				res.json({status: false, slide_id: null, code: "unknown"});
			} else {
				res.json({status: true, slide_id: slideId, code: null});
			}
		})
	})

	// Send to home page
	app.get("*", function(req, res){
		res.render(path.join(appViewsFolder, "pages/home"));
	})

mongoose.connect('mongodb://localhost/db/' + appInstanceName);
var db = mongoose.connection;

db.on('error', function(error){
	console.log('Failed to connect to MongoDB.', error);
	process.exit();
});

db.once('open', function() {
	var apps = {};
	apps[appInstanceName] = {
		// Remove 'http://' and trailing slash
		pattern: appUrlRoot.slice(7, -1),
		handler: app
	}
	http.createServer(virtualhost(apps)).listen(80);
});
