window.AppException = React.createClass({
	render: function() {
		return <div className="panel panel-default">
			<div className="panel-heading">
				<h3 className="panel-title">Ops, something went wrong...</h3>
			</div>
			<div className="panel-body">
				<p>
					We had an unexpected error. Please, try again in a few minutes.
				</p>
			</div>
			<div className="panel-footer">
				<div className="row">
				<div className="col-md-4 col-md-offset-8">
						<button src="/home" onClick={this.handleReload} className="btn btn-success">Reload app</button>
					</div>
				</div>
			</div>
		</div>
	},

	handleReload: function(){
		window.location = "/build";
	}
});