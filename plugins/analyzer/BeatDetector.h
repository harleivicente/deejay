#ifndef BEATDETECTOR_H
#define BEATDETECTOR_H
#include "SoundManager.h"
#include <list>
#include <iostream>

using std::list;

struct BeatData
{
    int time; // time in milliseconds
    float intensity;
    float intensity_rate; // intensity rate of change
    bool valid; // indicates if a beat is present
};

class BeatDetector
{
    public:
        BeatDetector(SoundManager* snd_mgr);
        ~BeatDetector();
        void audio_process ();
        float* get_energie1024();
        float* get_energie44100();
        float* get_energie_peak();
        float* get_conv();
        float* get_beat();
        int get_tempo();

    private:
        SoundManager* snd_mgr;
        int length;    // en PCM
        /*
            Size: number_of_samples / 1024.
            Each index is calculated as the average of 44100 samples (1 second).
        */
        float* energie1024;
        /*
            Size: number_of_samples / 1024.
            Each index is calculated as the average of 1024 samples (23.22 miliseconds).
        */
        float* energie44100;
        float* energie_peak; // les beat probables
        float* conv; // la convolution avec un train d'impulsions

        /*
            array of int of size number_of_samples / 1024

            0 if not a beat
            1 if a beat

            index i of array can be converted into miliseconds like:
            ith item refers to (i+1)*23.22 

            i.e

            (0) - refers to the 0-23 miliseconds
            (1) - the second to 23-46 miliseconds
            ....

        */
        float* beat; // la beat line
        int tempo; // le tempo en BPMs
        int energie(int* data, int offset, int window); // calcul l'energie du signal a une position et sur une largeur donn�e
        void normalize(float* signal, int size, float max_val); // reajuste les valeurs d'un signal � la valeur max souhait�e
        int search_max(float* signal, int pos, int fenetre_half_size); // recherche d'un max dans les parages de pos
        float* rate_of_energy_change; // list of energy rate of change
        /*
            Array of struct BeatData
        */
        BeatData* beats_data; 


        /*
            Makes sure that values are not bigger then 1.
        */
        void normalize_float_array(float* array, int size);
        /*
            Makes sure that every value is not bigger then 1.
        */
        void normalize_float_list(list<float>&);
        /*
            Populates BeatData* beats_data
        */
        void create_beat_data_lists();
        void create_rate_of_change_float_array(float* array, float* target, int size);
        /*
            Sends beat song analysis data through standard output
            @see main.cpp for format
        */
        void output_data();

};

#endif // BEATDETECTOR_H
