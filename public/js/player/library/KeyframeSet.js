/*
	Holds all the information about when images should be 
	changed.
*/
function KeyframeSet(keyframeSet, trim_start_ms){

	/*
		Array of:
			{t: <time in ms>, i: <strengh from 0 - 1>}
	*/
	this.set = keyframeSet;

	/*
		Finds initial keyframe in set
	*/
	this.initial_keyframe = this.findFirstKeyframeIndex(trim_start_ms);	
}

/*
	Obtain time of a keyframe

	@param index number
	@return mixed number | false
*/
KeyframeSet.prototype.getKeyframeTime = function(index){
	if(this.set[index]){
		return this.set[index].t;
	} else {
		return false;
	}
}

/*
	Finds the first keyframe within the keyframe set.

	@param trim_start_ms number - Time of soundtrack trim starts
	in milliseconds
*/
KeyframeSet.prototype.findFirstKeyframeIndex = function(trim_start_ms){
	var output = 0;
	for(var i =0; i < this.set.length; i++){
		var current = this.set[i].t;
		if(current < trim_start_ms) {
			output++;
		} else {
			break;
		}
	}

	if(output > 0 && !this.set[output]){
		return (output - 1);
	} else {
		return output;
	}
}
