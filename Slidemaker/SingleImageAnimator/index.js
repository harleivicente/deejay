 var winston = require('winston');

/*
	Single image animator

		This module is responsible for extracting data from
	the soundtrack and providing it in the form of 
	usabled information to the slide player. The
	genarated information is called keyframeSet and has
	the form:

		[{
			t: <ms>,
			i: <0 - 1>
		}, ...]

		Each object in the keyframeSet indicates the time,
	in milliseconds, of an transition point and the 
	intensity associated to that transition, normalized
	value.
		Multiple keyframeSets will be created for a given 
	soundtrack, each with a diferent number of transitions
	points. The keyframeSet to be used for a slideshow 
	should be selected based on the number of images 
	avaiable for the presentation. For more informaltion
	@see buildAllKeyframeSets(). 
		This module needs de analysis data of audio files
	produced by AudioAnalyzer module and should have
	the format:

		@param analysisData object. Data must be valid.
			{	
				// Length A
				energy: [<value>, ...],
				rate_of_energy: [<value>, ...],

				beats: [<milisecond>, ...],
				beats_intensity: [<value>, ...]
				rate_of_intensity: [<value>, ...]
			}

			@info
				Length A = (number of seconds * 44100) / 1024  	
				Each index represents (23.22 miliseconds)

			@info 
				<value> 0 - 1 with three digit precision
				(0, 0.001, ... 0.999, 1)
		
		@param options object - options to override GLOBAL settings
			{	
				// overrides global singleImageAnimationMinDelay
				minDelay: Mininum time delay between transition in ms
				// overrides global singleImageAnimationMaxDelay
				maxDelay: Maximum time delay
				// overrides global singleImageAnimationSpeedIncrements
				speedIncrements: Number of different transition speeds
				used while animating a particular keyframeSet
				// overrides global singleImageAnimationSpeedRatio
				speedRatio: ratio between amount of energy values speed 0
				is responsible for and for speed 1

			}
		  
	Transition (these are not beats. beats are not shown in 
	the diagram bellow)

	  |	
	  v	
	__|__|_|__|________|________|____|___|__|__|_
			  
			  |________| 
				  ||
				delays

*/
function SingleImageAnimator(analysisData, options) {
	if(!options) {options = {}}
	this.energy = analysisData.energy;
	this.beatTimes = analysisData.beats;
	this.beatIntensities = analysisData.beats_intensity;
	this.beatIntensityRates = analysisData.beats_intensity_rate;
	this.durationInMs = 1000 * Math.round((analysisData.energy.length * 1024) / 44100);

	if(typeof options.minDelay == "number")
		this.minDelay = options.minDelay; //ms
	else
		this.minDelay = singleImageAnimationMinDelay;
	if(typeof options.speedRatio == "number")
		this.speedRatio = options.speedRatio;
	else
		this.speedRatio = singleImageAnimationSpeedRatio;
	if(typeof options.maxDelay == "number")
		this.maxDelay = options.maxDelay; //ms
	else
		this.maxDelay = singleImageAnimationMaxDelay;
	if(typeof options.speedIncrements == "number")
		this.speedIncrements = options.speedIncrements; //ms
	else
		this.speedIncrements = singleImageAnimationSpeedIncrements;


	/*
		TransitionSpeeds define a relationship between audio beats
		and transition frequency.

		A speed of +2 means that after a transition two beats will
		be skipped. Vertical lines bellow are beats:

		__|__|__|__|__|__
		  ^        ^

	   	A speed of -1:

			1/(2^[1]) = .5 -> half of a beat will be skipped 
			before another transition
	   	
			______|_______|_______|__
				  ^   ^	  ^   ^   ^
	   	
	   	A speed of -2:
			
			1/(2^[2]) = .25 -> a quarter of a beat will be 
			skipped before another transition
			
			__|______________|__
			  ^    ^    ^    ^ 	
	*/
	this.minTransitionSpeed = null;
	this.maxTransitionSpeed = null;

	/*
		A transition speed combination is an array of transition speeds
		sorted in increasing order. Each speed comb. allows for the 
		creation of a keyframeSet. The transitions in a keyframeSet
		can only happen at the speeds listed in the 
		transitionSpeedCombination used to make it.

		Exemple speed combination: [7,6,5,4,3,2,1,-1,2]
	*/
	this.transitionSpeedCombinations = [];

	/*
		Created by _partitionNumberLine.
		Used by _getSpeedIndexAtTime.
	*/
	this.transitionSpeedMap = null;
	
	winston.debug("New SingleImageAnimator");
}

/*
	Calculates max/min transitions speeds, transition speed combinations.

	@return bool
*/
SingleImageAnimator.prototype._prepareData = function() {
	this.minTransitionSpeed = this._defineMinTransitionSpeed(); // fast
	this.maxTransitionSpeed = this._defineMaxTransitionSpeed(); // slow
	this.transitionSpeedCombinations = this._defineTransitionSpeedCombinations();
	this.transitionSpeedMap = this._partitionNumberLine(1,this.speedIncrements,this.speedRatio);

	var fail =
		this.minTransitionSpeed === false ||
		this.maxTransitionSpeed === false ||
		this.transitionSpeedCombinations === false ||
		this.transitionSpeedMap === false;

	return !fail;
}

/*
	Partitions a number line going from 0 to L into N parts.
	
			L1             L2				  Ln	
	|----------------1------------2  ...  --n----|
	
	The ratio betweens the lengths is as described below:
		L1/L2 = L2/L3 = .... Ln-1/Ln = ratio

	@param lineSize positive whole number
	@param nofPartitions integer - Number of partitions to create
	@param ratio number - Ratio defined as L1/L2 (where: 
		2 <= i <= number of partitions). For example, a ratio of 2
		means that every section is half the size of the one before.
	
	@return mixed
		array of number - 4 digit precision. The points where the number
		line was cut. 0 and L are not added. For example, for a number line
		going until 2 cut into 3 parts the return format would be:
		[1.210, 1.523]
			  OR
		false if error

	Used by _getSpeedIndexAtTime to decide what speed should be
	used at instant t. For example, if at time t the energy 
	is greater than marker 1 but lesser than marker 2 then speed L2
	will be used.
*/
SingleImageAnimator.prototype._partitionNumberLine = function(lineSize, nofPartitions, ratio) {
	winston.debug("_partitionNumberLine");

	/*
		Check params
	*/
	if(
		typeof lineSize != "number" ||
		lineSize <= 0 ||
		typeof nofPartitions != "number" ||
		nofPartitions <= 0 ||
		(nofPartitions - Math.floor(nofPartitions) > 0) ||
		typeof ratio != "number" ||
		ratio <= 0
	) {
		return false;
	}

	/*
		Generates function that models the function:

		ln = 	K^(N-1) * L
			   --------------
				K^(ln-1) * B

		where:

			- K: ratio
			- L: line size
			- N: number of partitions
			- ln: size partition n-th partition
				(n goes from 1 to N)

			- B = SUMMATION(K^i)
				i from 0 to (N-1)
	*/
	var fnGenerator = function(K, L, N){
		winston.silly("Generating length function...");

		// Calculating B
		var B = 0;
		for(var i = 0; i <= (N-1); i++){
			B += Math.pow(K, i);
		}
		winston.silly("Paramater B: " + B);


		return function(ln) {
			var numerator = Math.pow(K,N-1) * L;
			var denominator = Math.pow(K, ln-1) * B;
			return numerator/denominator;
		}
	}

	/*
		Collect length of all partitions
	*/
	var partitionLengths = [];
	var lengthCalculator = fnGenerator(ratio, lineSize, nofPartitions);
	for(var partitionIndex = 1; partitionIndex <= nofPartitions; partitionIndex++){
		partitionLengths.push(lengthCalculator(partitionIndex));
	}

	/*
		Calculating output
	*/
	var output = [];
	for(var i = 0; i < (partitionLengths.length - 1); i++){
		var outputItem = 0;
		for (var z = 0; z <= i; z++){
			outputItem += partitionLengths[z];
		}
		output.push(outputItem);
	}

	/*
		Truncate output to 4 digits
	*/
	for(var i=0; i<output.length; i++){
		output[i] = Number(output[i].toFixed(4));
	}

	winston.silly("Final output: " + output);

	return output;
}


/*
	Contructs multiple keyframeSets for the given data.

	@return mixed
		array of keyframeSet
		- keyframeSet: [<time in ms>, ...]
			OR
		false if error
*/
SingleImageAnimator.prototype.buildAllKeyframeSets = function(){
	var ready = this._prepareData();

	if(ready){
		var output = [];
		for(var i = 0; i < this.transitionSpeedCombinations.length; i++){
			var set = this._buildKeyframeSet(this.transitionSpeedCombinations[i]);
			if(set){
				output.push(set);
			} else {
				return false;
			}
		}
		return output;
	} else {
		return false;
	}
}

/*
	Constructs ONE keyframe set using the analysisData provided
	and for a particular 'transitionSpeedCombination'.
	
	@param transitionSpeedCombination - @see docs at begining of file
		Expects to have at least one speed
	@return mixed
		keyframeSet - @see docs at begining of file
			OR
		false if errors

*/
SingleImageAnimator.prototype._buildKeyframeSet = function(transitionSpeedCombination) {
	winston.silly("_buildKeyframeSet...");

	if(
		!Array.isArray(transitionSpeedCombination) ||
		transitionSpeedCombination.length <= 0 ||
		typeof transitionSpeedCombination[0] != "number"
	){
		return false;
	}

	/*
		Negative transition speeds means that beat is subdivided into N parts.
		N = 2 ^ (Abs(speed)).
		Check if last speed (fastest transition speed)
	*/
	var fastest = transitionSpeedCombination[transitionSpeedCombination.length - 1];
	if(fastest < 0){
		nofSubdivisions = Math.pow(2, Math.abs(fastest));
	} else {
		nofSubdivisions = 1;
	}
	winston.silly("	  * Number of subdivs: " + nofSubdivisions);

	// Output object
	var output = [];

	/*
		Analyze each beat defined in this.beatTimes.
		In case sub-beats exist consider them as well.
	*/
	winston.silly("	  * Number of beats: " + this.beatTimes.length);
	var timeSinceLastTriggerNormalized = 0;
	
	for(var beatI = 0; beatI < this.beatTimes.length; beatI++){
		winston.silly("			* beat #" + beatI);
	
		// Increment timer
		if(beatI !== 0)
			timeSinceLastTriggerNormalized += (1/nofSubdivisions);
	
		var beatTime = this.beatTimes[beatI];
		var trigger = false;

		/*
			Each beat/sub-beat will be analyzed according to the following
			rules:
				
				- first beat will always trigger transition
				- if at beat
					- speed rating negative:
						- have waited 100% or more of
						required beat percentage: TRIGGER 
					- speed rating positive
						- have waited 60% or more of 
						required beat percentage: TRIGGER
				- if at sub-beat:
					- speed rating positive: SKIP
					- speed rating negative
						- have waited 100% or more of
						required beat percentage: TRIGGER
		*/

		/*
			Beat
		*/
		if(beatI == 0){
			trigger = true;
		} else {
			var speedIndex = this._getSpeedIndexAtTime(beatTime, transitionSpeedCombination.length);

			// Failed
			if(speedIndex === false){
				return false;
			}

			var speedRating = transitionSpeedCombination[speedIndex];
			winston.silly("			> speed: " + speedRating + " - timer: " + timeSinceLastTriggerNormalized);
			if(speedRating > 0) {
				var targetWait = speedRating * 1;
				if(timeSinceLastTriggerNormalized >= targetWait * 1){
					trigger = true;
				}
			} else {
				var targetWait = 1/Math.pow(2, Math.abs(speedRating));
				if(timeSinceLastTriggerNormalized >= targetWait){
					trigger = true;
				}
			}
		}

		if(trigger){
			output.push({t: this.beatTimes[beatI], i: this.beatIntensities[beatI]});
			timeSinceLastTriggerNormalized = 0;
		} else {
			timeSinceLastTriggerNormalized += (1/nofSubdivisions);
		}


		/*
			Subdivisions.

			Last beat has no subdivisions
		*/
		winston.silly("			 	* sub parts");
		if(beatI < (this.beatTimes.length - 1)){
			for(var i = 1; i < nofSubdivisions; i++){
				// Increment timer
				timeSinceLastTriggerNormalized += (1/nofSubdivisions);

				// Length between current beat and next one
				var nextPreviousLength = this.beatTimes[beatI+1] - this.beatTimes[beatI];
				var subdivLength = nextPreviousLength / nofSubdivisions;
				var subdivisionTime = Math.round(beatTime + subdivLength*i);
				var speedIndex = this._getSpeedIndexAtTime(subdivisionTime, transitionSpeedCombination.length);
				var speedRating = transitionSpeedCombination[speedIndex];
				var targetWait = 1/Math.pow(2, Math.abs(speedRating));

				if(speedRating < 0 && timeSinceLastTriggerNormalized >= targetWait){
					output.push({t: subdivisionTime, i: this.beatIntensities[beatI]});
					timeSinceLastTriggerNormalized = 0;
				}
			}
		}
	}
	return output;
}

/*
	Calculates the transition speed index that should be used at any 
	given instant. Uses .transitionSpeedMap.

	@param time number - ms
	@param numberOfSpeeds number of transition speeds to choose from

	@return mixed
		int - Index of transition speed to be used
			OR
		false if any errors
*/
SingleImageAnimator.prototype._getSpeedIndexAtTime = function(time, numberOfSpeeds){
	winston.silly("_getSpeedIndexAtTime. time: " + time + ", numberOfSpeeds: " + numberOfSpeeds);
	if(
		!this.transitionSpeedMap ||
		typeof numberOfSpeeds != "number" ||
		numberOfSpeeds <= 0 ||
		(numberOfSpeeds - Math.floor(numberOfSpeeds) > 0) ||
		time > this.durationInMs
	){
		winston.silly("Failed.");
		return false;
	}
	winston.silly("	  * Passed basic requirements");

	/*
		Maps energy to one of N transition speeds according
		to transitionSpeedMap.
	*/
	var energy = this._getEnergyAtTime(time);
	winston.silly("	  * Energy at time: " + energy);
	var index = 0;
	for(var testIndex = 0; testIndex < this.transitionSpeedMap.length; testIndex++){
		if(energy >= this.transitionSpeedMap[testIndex]) {
			index++;
		} else {
			break;
		}
	}
	winston.silly("	  * Speed index: " + index);

	return index;
}

/*
	Obtains energy level at certain time.

	@param time int
	@return int energy level
*/
SingleImageAnimator.prototype._getEnergyAtTime = function(time) {
	var index = Math.floor(time / 23);
	return this.energy[index];
}

/*
	Populates .transitionSpeedCombinations

	@return mixed - array [<transition speed combination>]
		where <transition speed combination> - [<transition speed>,...] sorted
		in increasing order.
		or false if error
*/
SingleImageAnimator.prototype._defineTransitionSpeedCombinations = function(){
	if 	(
			!this.maxTransitionSpeed || // number but not zero
			!this.minTransitionSpeed || // number but not zero
			this.maxTransitionSpeed < this.minTransitionSpeed ||
			(Math.abs(this.maxTransitionSpeed) + Math.abs(this.minTransitionSpeed)) < this.speedIncrements
		){
		return false;
	} else {
		winston.debug("_defineTransitionSpeedCombinations...");
		var output = [];

		var numberOfSpeeds = this.maxTransitionSpeed - this.minTransitionSpeed + 1;
		/*
			If 0 was included remove it
		*/
		if(this.maxTransitionSpeed * this.minTransitionSpeed < 0){
			numberOfSpeeds--;
		}

		var numberOfCombinations = numberOfSpeeds - this.speedIncrements + 1;
		winston.silly("Number of combinations to calculate: " + numberOfCombinations);

		for(var combNumber = 1; combNumber <= numberOfCombinations; combNumber++){
			var leftBound = this.maxTransitionSpeed - combNumber + 1;
			/*
				Zero is not a valid transition speed. So if
				left bound has gone over 0 add 1.
			*/
			if(leftBound == 0 || (this.maxTransitionSpeed * leftBound) < 0) {
				leftBound++;
			}

			winston.silly("...calculating combination #" + combNumber);
			winston.silly("		> leftBound: " + leftBound);

			var combination = [];
			for(var i = leftBound; combination.length < this.speedIncrements; i--){
				if(i === 0){
					continue;
				} else {
					combination.push(i);
				}
			}

			// Fail if unable to create combinations of correct size
			if(combination.length != this.speedIncrements){
				return false;
			}
			output.push(combination);
		}
		return output;
	}
}

/*
	Finds the slowest transition speed (least amount of time 
	between image change) that respects minDelay/maxDelay.

	@return mixed - int ... 3, 2, 1, -1, -2 ... or FALSE if error
*/
SingleImageAnimator.prototype._defineMaxTransitionSpeed = function(){
	var longestTimeBetweenBeats = this._findLongestTimeBetweenBeats();
	winston.debug("_defineMaxTransitionSpeed...");
	winston.silly("longestTimeBetweenBeats = " + longestTimeBetweenBeats);

	/*
		Beats are too far apart (more than max allowed). Have to split them.
	*/
	if(longestTimeBetweenBeats > this.maxDelay){
		winston.silly("longest time bewteen beats is greater than maxDelay");
		// Attempt to subdivide up to 4 times
		for(var i = 1; i <= 4; i++) {
			var newTime = longestTimeBetweenBeats * (1/Math.pow(2,i));
			winston.silly("testing subdivision level: " + i + " of length: " + newTime);

			if(newTime <= this.maxDelay){
				return i * -1;
			// Even at max subdivisions subbeats are too far apart
			} else if (i == 4) {
				return false;
			}
		}
	/*
		Beats are too close together, may skip a few.
	*/
	} else {
		var testedSkipLevel = 0;
		// Atempt to skip as many as 100 beats
		for(var i = 1; i <= 100; i++) {
			var newTime = longestTimeBetweenBeats * i;

			if(newTime <= this.maxDelay){
				testedSkipLevel = i;
			} else {
				break;
			}
		}
		return testedSkipLevel;		
	}

}

/*
	Finds the fastest transition speed (least amount of time 
	between image change) that respects minDelay/maxDelay.

	@return mixed - int ... 3, 2, 1, -1, -2 ... or FALSE if error
*/
SingleImageAnimator.prototype._defineMinTransitionSpeed = function(){
	var shortestTime = this._findShortestTimeBetweenBeats();
	winston.debug("_findShortestTimeBetweenBeats...");
	winston.silly("shortest time bewteen beats: " + shortestTime);

	/*
		All beats are farther apart than minimum requirement.
		May be able to subdivide beats.
	*/
	if(shortestTime > this.minDelay){
		winston.debug("may be able to split beats.");
		var testedSubdivionLevel = 0;
		// Atempt several subdivision levels and check if valid (up to 4)
		for(var i = 1; i <= 4; i++) {
			var newShortestTime = shortestTime * (1/Math.pow(2,i));

			if(newShortestTime >= this.minDelay){
				testedSubdivionLevel = i;
			} else {
				break;
			}
		}

		// If not able to subdivide than min speed is 1.
		if(testedSubdivionLevel == 0){
			return 1;
		} else {
			return -1 * testedSubdivionLevel;
		}

	/*
		Beats are very close together. Might have to skip a few.
	*/
	} else {

		// Attempt to skip the least beats possible
		for(var i = 0; i <= 100; i++){
			var newTime = shortestTime * (i + 1);
			if(newTime >= this.minDelay) {
				break;

			// Reached max without success
			} else if(i == 100){
				return false;
			}
		}

		return (i + 1);
	}
}

/*
	Search through this.beatTimes to find the shortest time between two beats.

	@return int - Time in ms
*/
SingleImageAnimator.prototype._findShortestTimeBetweenBeats = function(){
	var shortestTimeSoFar = -1;
	for(var i = 1; i < this.beatTimes.length; i++){
		var time = this.beatTimes[i] - this.beatTimes[i-1];
		if(shortestTimeSoFar < 0 || time < shortestTimeSoFar){
			shortestTimeSoFar = time;
		}
	}
	return shortestTimeSoFar;
}

/*
	Search through this.beatTimes to find the longest time between two beats.

	@return int - Time in ms
*/
SingleImageAnimator.prototype._findLongestTimeBetweenBeats = function(){
	var longestTimeSoFar = -1;
	for(var i = 1; i < this.beatTimes.length; i++){
		var time = this.beatTimes[i] - this.beatTimes[i-1];
		if(longestTimeSoFar < 0 || time > longestTimeSoFar){
			longestTimeSoFar = time;
		}
	}
	return longestTimeSoFar;
}

module.exports = SingleImageAnimator;