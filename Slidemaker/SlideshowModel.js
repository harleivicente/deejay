var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var async = require("async");
var FacebookImageSetModel = require('./FacebookImageSet/FacebookImageSetModel');
var FacebookImageSet = require('./FacebookImageSet');
var ModuleUtil = require('./ModuleUtil');

var schema = new Schema({
	audioSource: {type: String, enum:["youtube"], required: true},
	audioSourceId: {type: Schema.Types.ObjectId, required: true},
	trim: {
		start: {type: Number},
		end: {type: Number}
	},
	imageSource: {type: String, enum: ["facebook"], required: true},
	imageSourceId: {type: Schema.Types.ObjectId, required: false},
	/*
		Indicates which keyframeSet in the audio source
		object to use for this slideshow. This value 
		is calculated when slideshow is created using
		the "insert" instance method.
	*/
	keyframeSetIndex: {type: Number}
});

/*
	@param options object {
			audio_source,
			audio_id
			start_time <ms>
			end_time <ms>
			image_set_source [facebook]
			image_set_id 
		}

	@param callback function(error, slideId)
*/
schema.statics.insert = function(options, callback) {
	/*
		Obtain number of images being used
	*/
	if(options.image_set_source == "facebook"){
		var ImageSetModel = require("./FacebookImageSet/FacebookImageSetModel");
	}
	var audioModuleName = ModuleUtil.getModuleName(options.audio_source, "audio");
	var AudioModule = require("./"+audioModuleName);

	async.waterfall([
		function(cb){
			ImageSetModel.findById(options.image_set_id, cb)
		},
		function(imageSet, cb){
			cb(!imageSet, imageSet.images.length);
		},
		function(nofImages, cb){
			AudioModule.getBestKeyframeSetIndex(
				options.audio_id, options.start_time,
				options.end_time, nofImages, cb)
		},
		function(keyframeIndex, cb){
			var item = new SlideshowModel({
				audioSource: options.audio_source,
				audioSourceId: options.audio_id,
				keyframeSetIndex: keyframeIndex,
				trim: {
					start: options.start_time,
					end: options.end_time
				},
				imageSource: options.image_set_source,
				imageSourceId: options.image_set_id
			});
			item.save(cb);
		}
	],
	function(error, slide){
		if(error){
			callback(error, null);
		} else {
			callback(null, slide._id);
		}
	});
}

/*
	Removes a slideshow and all data associated to it.

	@param slideId string
	@param callback function(Error)
		Errors:
			null
			unknown-error

*/
schema.statics.removeComplete = function(slideId, callback){
	SlideshowModel.findById(slideId, function(err, item){
		var facebook = item.imageSource == "facebook";
		if(err || !item || !facebook){
			callback(new Error("unknown-error"));
		} else {
			async.parallel([
				function(callback){
					FacebookImageSet.remove(item.imageSourceId, callback);
				}
				], function(error){
					if(error){
						callback(new Error("unknown-error"));
					} else {
						SlideshowModel.findByIdAndRemove(slideId, function(error){
							if(error) {
								callback(new Error("unknown-error"));
							} else {
								callback(null);
							}
						});
					}
				}
			);
		}
	});
}

var SlideshowModel = mongoose.model('SlideshowModel', schema);
module.exports = SlideshowModel;


