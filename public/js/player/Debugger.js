/*
	Debugger properties

	@prop main
*/

window.Debugger = React.createClass({
	getInitialState: function(){

		// Given local state pointer to parent
		this.props.main.setState({debugger: this});

		var instance = this;
		return {
			avg_keyframe_error: 0,
			skipped_keyframes: 0,
			max_beat_identifier_error: 0,
			avg_beat_identifier_error: 0,
			displayed_indicator_beats: 0
		};
	},
	render: function(){
		return <div id="dev-module" className="debugger hide">
			<ul className="list-group">
			  <li className="list-group-item">
			    <span className="badge">{this.state.avg_keyframe_error} ms</span>
			    Average keyframe delay
			  </li>
			  <li className="list-group-item">
			    <span className="badge">{this.state.skipped_keyframes}</span>
			    Skipped keyframes
			  </li>
			  <li className="list-group-item">
			    <span className="badge">{this.state.max_beat_identifier_error.toFixed(2)} ms</span>
			    Maximum beat identifier error
			  </li>
			  <li className="list-group-item">
			    <span className="badge">{this.state.avg_beat_identifier_error.toFixed(2)} ms</span>
			    Average beat identifier error
			  </li>  
			  <li className="list-group-item">
			    <span className="badge">{this.state.displayed_indicator_beats}</span>
			    Number of beats displayed by indicator
			  </li>    
			</ul>
		</div>
	}
});


