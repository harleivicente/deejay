var expect = require('chai').expect;
var path = require("path");
var AudioAnalyzer = require("../AudioAnalyzer");
var test_data_folder = path.join(appRoot, "Slidemaker", "tests", "_data");

describe('AudioAnalyzer analyze()', function() {

	it("should return the error: file-missing if file given does not exist", function (done) {
		AudioAnalyzer.analyze("fakefile", function(error, data){
			expect(error).to.be.an('error');
			expect(error.message).to.equal("file-missing");
			expect(data).to.be.null;
			done();
		});
	});

	it("should return the error 'invalid-file' if file is not a mp3 file", function (done) {
		var file = path.join(test_data_folder, "fake_audio_file.mp3");
		AudioAnalyzer.analyze(file, function(error, output){
			expect(error).to.be.an("error");
			expect(error.message).to.be.equal("invalid-file");
			expect(output).to.be.null;
			done();
		});
	});

	it("should return analysis data if file is a valid song", function (done) {
		var file = path.join(test_data_folder, "valid_audio.mp3");
		AudioAnalyzer.analyze(file, function(error, output){
			expect(error).to.be.null;
			expect(output).to.be.a("string");
			var attempt_parse = function(){
				JSON.parse(output);
			}
			expect(attempt_parse).to.not.throw(Error);
			done();
		});
	});

	it("should return analysis data if file is a valid audio but not musical", function (done) {
		var file = path.join(test_data_folder, "not_musical.mp3");
		AudioAnalyzer.analyze(file, function(error, output){
			expect(error).to.be.null;
			expect(output).to.be.a("string");
			var attempt_parse = function(){
				var parsed = JSON.parse(output);
			}
			expect(attempt_parse).to.not.throw(Error);
			done();
		});
	});

});

