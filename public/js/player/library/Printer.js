/*
	Responsible for drawing into the canvas. Should be called 
	from within the 'render' functions of Rekapi actors.

*/
function Printer(context2d) {
	this.context = context2d;
}

/*
	@param images_details array of
	{
		index: <Images.data index>,
		img_xs: <>, img_ys: <>,
		img_width: <>, img_height: <>,
		x: <relative to image frame>, y: <relative to image frame>,
		width: <>, height: <>
	}
*/
Printer.prototype.printImages = function(images_details) {
	for(var i in images_details){
		var image_detail = images_details[i];
		var frame_zero = manager.dimensions.fetch("frame_zero");
		this.context.drawImage(
			manager.images.solo_images[image_detail.index],
			image_detail.img_xs,
			image_detail.img_ys,
			image_detail.img_width,
			image_detail.img_height,
			image_detail.x + frame_zero.x,
			image_detail.y + frame_zero.y,
			image_detail.width,
			image_detail.height
		);
	}
}

/*
	Prints beat indicator / debug area
*/
Printer.prototype.printBeatIndicator = function(){
	var pos = manager.dimensions.fetch("beat_indicator_zero");
	var width = manager.dimensions.fetch("beat_indicator_width");
	var height = manager.dimensions.fetch("beat_indicator_height");	
	var centerX = pos.x + width/2;
	var centerY = pos.y + height/2;

	this.context.fillStyle = "#474747";
	this.context.fillRect(pos.x, pos.y, width, height);
	this.context.beginPath();
	this.context.arc(centerX, centerY, manager.beatTracker.indicator_radius, 0, 2 * Math.PI, false);
	this.context.fillStyle = "#23C882";
	this.context.fill();
	this.context.lineWidth = 1;
	this.context.strokeStyle = '#fff';
	this.context.stroke();
}

Printer.prototype.printStageBg = function(){
	this.context.fillStyle = "#0C0C0C";
	var stage_pos = manager.dimensions.fetch("stage_zero");
	var stage_width = manager.dimensions.fetch("stage_width");
	var stage_height = manager.dimensions.fetch("stage_height");
	this.context.fillRect(stage_pos.x, stage_pos.y, stage_width, stage_height);
}
