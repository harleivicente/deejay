/*
	Handles download from youtube site into server +
	conversion into mp3.

*/

var path = require("path");	
var execFile = require("child_process").execFile;

/*
	Attempts to download youtube video into folder: ./public/youtube
	File is saved as <base_filename>.<ext>. Converts into mp3 format.

	@param video_code string
	@param base_filename string - filename without extension
	@param callback function(Error)
		> unknown-error

*/
function download(video_code, base_filename, callback) {
		var youtube_url = "https://www.youtube.com/watch?v=" + video_code;
		var command_file = path.join(appRoot, "plugins", "youtube-dl");
		var encoder_path = path.join(appRoot, "plugins", "ffmpeg");
		var args_list = [
			"-f",
			"[height<=480]",
			"-x",
			"--audio-format",
			"mp3",
			"-o",
			appRoot + "/public/youtube/" + base_filename + ".%(ext)s",
			youtube_url
		];
		var options = {
			env: {
				// PATH: process.env.PATH + ":" + encoder_path
			}
		};
		execFile(command_file, args_list, options, function(error, stdout, stderr){
			if(error){
				console.log(error);
				callback(new Error("unknown-error"));
			} else {
				callback(null);
			}
		});
}

module.exports.download = download;