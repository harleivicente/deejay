var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var path = require("path");
var util = require("util");
var moment = require("moment");
var async = require("async");
var fbgraph = require("fbgraph");

fbgraph.setOptions({
    timeout:  10000
  , pool:     { maxSockets:  Infinity }
  , headers:  { connection:  "keep-alive" }
});

var model = new Schema({
	facebookUserId: {type: String, required: true},
	images: [String],
});

/*
	Insert facebook image set

	@param fbUserId string
	@param imageIds [] | null
	@param id mongoose.Types.ObjectId
	@param callback function(Error, id)
		Errors: 
			null
			unknown-error
*/
model.statics.insert = function(fbUserId, imageIds, id, callback){
	if(!fbUserId){
		callback(new Error("unknown-error"), null);
	} else {

		var item = new FacebookImageSetModel({
			facebookUserId: fbUserId,
			images: imageIds,
			_id: id
		});

		item.save(function(error, item){
			if(error){
				callback(new Error("unknown-error"), null);
			} else {
				callback(null, item._id);
			}
		});
	}
}

/*

	Obtains image uris.

	Images are saved in folder ./public/slide_data/facebook_images
	and are named: "facebook-user-id"_"image-id"

	@param imageSetId string
	@param callback function(Error, imageUris)

	Erros:
		null
		unknown-error

	imageUris: [<string>, ...]
*/
model.statics.getUris = function(imageSetId, callback){
	FacebookImageSetModel.findById(imageSetId, function(error, item){
		if(error || !item){
			callback(new Error("unknown-error"), null);
		} else {
			var uris = [];

			for(var i =0; i < item.images.length; i++){
				uris.push(appUrlRoot + "slide_data/facebook_images/" + imageSetId + item.images[i]);
			}

			callback(null, uris);
		}
	})
}

try {
	var FacebookImageSetModel = mongoose.model("FacebookImageSetModel")
} catch(e) {
	var FacebookImageSetModel = mongoose.model('FacebookImageSetModel', model);
}

module.exports = FacebookImageSetModel;




