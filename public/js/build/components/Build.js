window.Build = React.createClass({

	getInitialState: function() {
		return {
			building: false
		}
	},

	render: function() {
	var main = this.props.main;
	var building = this.state.building;
	var error = this.state.error;

		if(this.state.building){
			var footer = <div className="row">
				<div className="col-sm-12">
					<p><strong>Building your slideshow...</strong></p>
					<div id="progress-bar"></div>
				</div>
				</div>;
		} else {
			var footer = <div className="row">
					<div className="col-sm-4">
						<button type="button" onClick={this.handlePrevious} className="btn btn-default">Back</button>
					</div>
					<div className="col-sm-4 col-sm-offset-4">
						<button type="button" onClick={this.handleBuild} className="btn btn-success">Build</button>
					</div>
				</div>;
		}

		return <div className="panel panel-default">
			<div className="panel-heading">
				<h3 className="panel-title">Slideshow overview</h3>
			</div>
			<div className="panel-body">
				<div className="overview">
					<p>
						<span className="glyphicon glyphicon-music" aria-hidden="true"></span>
						<span className="soundtrack-name">Soundtrack title: {main.state.audio_title}</span>
					</p>
					<p className="soundtrack-trim-length">
						<span>Soundtrack length: {this.getSoundtrackTrimLength()}</span>
					</p>
					<p>
						<span className="glyphicon glyphicon-picture" aria-hidden="true"></span>
						{main.state.image_ids.length} Image(s) selected.
					</p>
				</div>

				<div className="overview confirm">
					<p><strong>If you are ready to complete the slideshow click bellow.</strong></p>
				</div>

			</div>
			<div className="panel-footer">
				{footer}
			</div>
		</div>
	},

	handlePrevious: function() {
		var main = this.props.main;
		main.setState({
			stage: "choose_images"
		});
	},

	handleBuild: function() {
		var building = this.state.building;
		var main = this.props.main;
		var instance = this;

		if(!building){
			this.setState({
				building: true
			});

			$.ajax({
				url: "http://" + window.location.host + "/slideshows",
				type: "POST",
				data: {
					audio_source: "youtube",
					audio_id: main.state.audio_id,
					start_time: main.state.audio_time_start,
					end_time: main.state.audio_time_end,
					image_set_source: "facebook",
					image_set_config: {
						imageIds: main.state.image_ids,
						facebookUserId: FB.getUserID()
					}
				}
			})
			.done(function(result){
				if(result.status){
					window.location = "http://" + window.location.host + "/slides/" + result.slide_id + "/play";
				} else {
					main.setState({
						stage: "app_exception"
					});	
				} 
			})
			.error(function(){
				main.setState({
					stage: "app_exception"
				});	
			});			
		}
	},

	/*
		@return string trimmed length in seconds in format mm:ss
	*/
	getSoundtrackTrimLength: function(){
		var main = this.props.main;
		var length = main.state.audio_time_end - main.state.audio_time_start;
		return moment.utc(length * 1000).format("mm:ss");
	},

	componentDidUpdate: function() {
		$("#progress-bar").html("<div class='progress'><div class='progress-bar progress-bar-success progress-bar-striped active' role='progressbar'></div></div>");
		$(".progress-bar").attr("style", "width:100%");
	},

	/*
		Sends request to server to build slideshow.
	
	*/
	buildSlide: function(){

	}


});