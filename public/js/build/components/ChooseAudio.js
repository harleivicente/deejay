window.ChooseAudio = React.createClass({
	getInitialState: function() {
		return {
			hasError: false,
			errorMsg: "",
			loading: false,
			showTips: false
		}
	},
	render: function() {
		var class_name = this.state.hasError ? "form-group has-error" : "form-group";
		
		if(this.state.loading){
			var footer = <div className="row">
				<div className="col-sm-12">
					<p><strong>Analyzing audio. This might take a few moments. </strong></p>
					<div id="progress-bar"></div>
				</div>
			</div>;
		} else {
			var footer = <div className="row">
				<div className="col-sm-4 col-sm-offset-8">
						<button type="button" onClick={this.handleNext} className="btn btn-success">Analyze audio</button>
					</div>
				</div>
		}

		return <div className="panel panel-default">
			<div className="panel-heading">
				<h3 className="panel-title">Choose audio</h3>
			</div>
			<div className="panel-body">
				<p>
					Provide the url of a Youtube video that has the soundtrack that you would like to use. Videos longer than 12 minutes are not allowed.
				</p>

				<form>
					<div className={class_name}>
						<label className="control-label" for="youtube-url"><img src="/images/youtube.png" className="youtube-icon"></img>Video url</label>
						<input type="text" className="form-control" id="youtube-url" placeholder="https://www.youtube.com/watch?v=fnVU9a3Pqcc" disabled={this.state.loading}/>
						<span id="youtube-url" className="help-block">
							{this.state.errorMsg}
						</span>
					</div>
				</form>

				<ul className="tips" style = {{display: this.state.showTips ? "" : "none"}}>
					<li> abc </li>
					<li> abc2 </li>
					<li> abc2 </li>
				</ul>
			</div>
			<div className="panel-footer">
				{footer}
			</div>
		</div>
	},
	handleNext: function(e) {
		var main = this.props.main;
		var youtube_url = $("#youtube-url").val();
		var video_id = extract_youtube_video_id(youtube_url);
		var instance = this;

		if(video_id){
			this.setState(
				{
					loading: true,
					hasError: false
				}
			);

			async.waterfall([
				function(cb){
					youtube_get_video_info(video_id,function(error, result){
						if(error){
							cb(false, false, null);
						} else {
							main.setState({
									audio_title: result.title,
									audio_length: result.length
							});
							cb(false, true, result.length > (12 * 60));
						}
					});
				},
				function(videoExists, videoTooLong, cb){
					if(!videoExists){
						instance.setState(
							{
								loading: false,
								hasError: true,
								errorMsg: "This does not seem to be a valid youtube video."
							}
						);
						cb(false);
					} else if(videoTooLong){
						instance.setState(
							{
								loading: false,
								hasError: true,
								errorMsg: "Videos cannot be longer than 12 minutes."
							}
						);
						cb(false);
					} else {
						request_youtube_audio_analysis(video_id, function(error, audio_id, invalid, audio_uri){
							// exception
							if(error){
								main.setState({
									stage: "app_exception"
								});
							} else if (invalid) {
								instance.setState(
									{
										loading: false,
										hasError: true,
										showTips: true,
										errorMsg: "We were unable to analyze this audio. Please, choose another one."
									}
								);
								cb(false);
							} else {
								main.setState({
									stage: "config_audio",
									audio_id: audio_id,
									audio_uri: audio_uri
								});
								cb(false);
							}
						});
					}
				}
			]);
		} else {
			instance.setState(
				{
					hasError: true,
					errorMsg: "Make sure to insert a valid youtube url."
				}
			);
		}
	},

	componentDidUpdate: function() {
		$("#progress-bar").html("<div class='progress'><div class='progress-bar progress-bar-success progress-bar-striped active' role='progressbar'></div></div>");
		$(".progress-bar").attr("style", "width:100%");
	}
});