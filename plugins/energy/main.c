#include <stdio.h>
#include <stdlib.h>  
#include <aubio/aubio.h>

aubio_source_t *this_source = NULL;
uint_t block_number = 0;
uint_t read = 0;
fvec_t *ibuf;
smpl_t value;
fvec_t *collected_values;

/*
	How many samples to average.

*/
uint_t samplerate = 44100;
uint_t hop_size = 1024;



/*
	Calculates instantaneous energy (every 1024 samples).

	@usage ./main <path-to-wav-file>

*/
int main(int argc, char **argv) {

	FILE *fp;
	char path[1035];
	/* Open the command for reading. */
	fp = popen("/bin/ls /etc/", "r");
	if (fp == NULL) {
		printf("Failed to run command\n" );
		exit(1);
	}

	/* Read the output a line at a time - output it. */
	while (fgets(path, sizeof(path)-1, fp) != NULL) {
		printf("%s", path);
	}
	/* close */
	pclose(fp);


	/*
		Openning file and getting file size
	*/
	this_source = new_aubio_source ((char_t*)argv[1], samplerate, hop_size);

	// Create input buffer
	ibuf = new_fvec (hop_size);



	if(this_source == NULL){
		exit(1);
	} else {
		/*
			Openning output json
		*/
		printf("[");

	    uint_t total_read = 0;

	    do {
      		aubio_source_do (this_source, ibuf, &read);
	      
			value = aubio_level_lin(ibuf);

			if(block_number > 0){
				printf(",%f", value);
			} else {
				printf("%f", value);
			}

			total_read += read;
			block_number++;

	    } while (read == hop_size);

	    /*
			Closing output json 
	    */
		printf("]");
	}

}

/*
	Normalizes values of collected_values
	so values are between 0 and 1.

*/
void normalize_data() {

}