var expect = require('chai').expect;
var ModuleUtil = require("../ModuleUtil");

describe('ModuleUtil', function() {

	describe('getModuleName()', function () {

		it("should throw the error: invalid-source-type if source type is not audio or image", function () {
			var response = ModuleUtil.getModuleName("facebook", "auadfadfddio");
			expect(response).to.be.an('error');
			expect(response.message).to.equal("invalid-source-type");
		});

		it("should throw the error: invalid-source-name if source name is not defined in the app config file", function(){
			var audioResponse = ModuleUtil.getModuleName("madeupthing", "audio");
			var imgResponse = ModuleUtil.getModuleName("madeupthing", "image");
			expect(audioResponse).to.be.an('error');
			expect(imgResponse).to.be.an('error');
			expect(audioResponse.message).to.equal("invalid-source-name");			
			expect(imgResponse.message).to.equal("invalid-source-name");			
		})

		it("should return YoutubeAudio when source type is audio and input source name is youtube", function(){
			var response = ModuleUtil.getModuleName("youtube", "audio");
			expect(response).to.be.an('string');
			expect(response).to.be.equal('YoutubeAudio');
		})

		it("should return <name>ImageSet when source type is image and input source name is <name>", function(){
			var response = ModuleUtil.getModuleName("facebook", "image");
			expect(response).to.be.an('string');
			expect(response).to.be.equal('FacebookImageSet');
		})

	});

});

