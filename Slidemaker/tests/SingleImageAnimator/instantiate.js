var expect = require('chai').expect;
var SingleImageAnimator = require('../../SingleImageAnimator');
var sampleAnalysisData = {
	energy: [10, 20, 10, 5],
	rate_of_energy: [10, 10, 10, 5],
	beats: [20],
	beats_intensity: [10],
	beats_intensity_rate: [10]	
};

describe('new() SingleImageAnimator', function() {

	it("It throws an exception if no analysis data given", function(){
		expect(function(){new SingleImageAnimator()}).to.throw(Error);
	});

	it("It does not throw an exception if proper analysis data is given", function(){
		expect(function(){new SingleImageAnimator(sampleAnalysisData)}).to.not.throw(Error);
	});

	it("Creates an proper instance and overrides the params passed in.", function(){
		var options = {
			minDelay: 1000,
			// maxDelay: not changed
			speedIncrements: 20
		};
		var instance = new SingleImageAnimator(sampleAnalysisData, options);
		expect(instance.minDelay).to.equal(1000);
		expect(instance.maxDelay).to.equal(singleImageAnimationMaxDelay);
		expect(instance.speedIncrements).to.equal(20);
	})

});

