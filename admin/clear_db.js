var mongoose = require("mongoose");
var YoutubeModel = require("../Slidemaker/YoutubeAudio/YoutubeAudio");

mongoose.connect('mongodb://localhost/db');
var db = mongoose.connection;

db.on('error', function(){
	console.log('Failed to connect to MongoDB.');
	process.exit();
});

db.once('open', function() {
	YoutubeModel.remove({}, function(error){
		if(error){
			console.log("Unable to clear database: YoutubeModel");
		} else {
			console.log("Cleared database database: YoutubeModel");
		}
		process.exit();
	});
});

