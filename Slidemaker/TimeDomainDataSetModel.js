/*

	Single dimension data for time domain.

	I.e music energy, tempo

*/

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var schema = new Schema({
	
	/*
	
		Json of:
		
		[<value>, ...] for data sampled evenly, random
		set to false

		or 

		[{t: <time in ms>, v: <value>}, ...] for data with
		random sampling, random set to true

	*/
	data: String,
	random: Boolean,
	sampleRate: {type: Number, required: true},
	/*
		Unique name for data set
	*/
	name: {type: String, index: { unique: true}}

});


var TimeDomainDataSet = mongoose.model("TimeDomainDataSet", schema);
module.exports = TimeDomainDataSet;