/*
	Holds all the information about detected beats

	@param beatSet object
		{
			times: [<ms>, ...]
			intensities: [<ms>, ...]
		}

	@param trim_start_ms number - soundtrack starting trim point in ms
*/
function BeatSet(beatSet, trim_start_ms){

	this.times = beatSet.times;
	this.intensities = beatSet.intensities;

	/*
		Finds initial beat index
	*/
	this.initial_beat_index = this.findFirstBeatIndex(trim_start_ms);	
}

BeatSet.prototype.getBeatTime = function(index){
	return this.times[index];
}

BeatSet.prototype.getBeatIntensity = function(index){
	return this.intensities[index];
}

/*
	Finds the first beat within the set that matches the soundtrack
	trim start point.

	@param trim_start_ms number - Time of soundtrack trim starts
	in milliseconds
*/
BeatSet.prototype.findFirstBeatIndex = function(trim_start_ms){
	var output = 0;
	for(var i =0; i < this.times.length; i++){
		var current = this.times[i];
		if(current < trim_start_ms) {
			output++;
		} else {
			break;
		}
	}

	if(output > 0 && !this.times[output]){
		return (output - 1);
	} else {
		return output;
	}
}
