var FacebookImageSet = require("../../FacebookImageSet");
var FacebookImageSetModel = require("../../FacebookImageSet/FacebookImageSetModel");
var expect = require('chai').expect;
var rewire = require("rewire");
var async = require("async");
var path = require("path");
var testUtility = require("./testUtility");

describe('FacebookImageSet remove()', function() {
	
	beforeEach(function(done){
		testUtility.clearAllFbImageSets(function(error){
			expect(error).to.be.null;
			done();
		})
	})

	afterEach(function(done){
		testUtility.clearAllFbImageSets(function(error){
			expect(error).to.be.null;
			done();
		})
	})

	it("Returns unknown-error if given id is invalid", function(done){
		FacebookImageSet.remove("fakeidhere", function(error){
			expect(error).to.be.an("error");
			expect(error.message).to.equal("unknown-error");
			done();
		})
	});

	it("Removes database entry and images from server if args are valid.", function(done){
		var options = {
			facebookUserId: testConfig.fbUserId,
			images: testConfig.fbImages
		};
		FacebookImageSet.create(options, function(error, imageSetId){
			expect(error).to.be.null;

			FacebookImageSet.remove(imageSetId, function(error){
				expect(error).to.be.null;

				// Check database removal
				FacebookImageSetModel.findById(imageSetId, function(error, item){
					expect(item).to.be.null;
				}).then(function(){
					// Check if files were removed
					async.map(testConfig.fbImages,
					function(imageId, cb){
						var file = path.join(
							appRoot, "public",
							"slide_data", "facebook_images",
							imageSetId + imageId);
						testUtility.file_exists(file, function(error, exists){
							cb(exists);
						});
					},
					function(error){
						expect(error, "Images were not remove from server.").to.not.be.true;
						done();
					}
				);
				});
			})
			
		});
	});


});
