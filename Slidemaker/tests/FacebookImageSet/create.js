var expect = require('chai').expect;
var FacebookImageSetModel = require("../../FacebookImageSet/FacebookImageSetModel");
var FacebookImageSet = require("../../FacebookImageSet");
var async = require("async");
var path = require("path");
var testUtility = require("./testUtility");

describe('FacebookImageSet create()', function() {

	beforeEach(function(done){
		testUtility.clearAllFbImageSets(function(error){
			expect(error).to.be.null;
			done();
		})
	})

	afterEach(function(done){
		testUtility.clearAllFbImageSets(function(error){
			expect(error).to.be.null;
			done();
		})
	})

	it("Saves images locally and returns no error when given valid data", function(done){
		var options = {
			facebookUserId: testConfig.fbUserId,
			images: testConfig.fbImages
		};
		FacebookImageSet.create(options, function(error, imageSetId){
			expect(error).to.be.null;
			expect(imageSetId).to.not.be.null;

			// Check if there is database entry
			FacebookImageSetModel.findById(imageSetId, function(error, item){
				expect(error).to.be.null;
				expect(item).to.not.be.null;
				expect(item.images.length).to.equal(testConfig.fbImages.length);

				// Check if files named <imageSetId> + <imageId> exist for each image
				async.map(testConfig.fbImages,
					function(imageId, callback){
						var file = path.join(
							appRoot, "public",
							"slide_data", "facebook_images",
							imageSetId + imageId);
						testUtility.file_exists(file, function(error, exists){
							expect(error).to.be.null;
							expect(exists).to.be.true;
							callback(null);
						});
					},
					function(error){
						done();
					}
				);
			});

		});
	});

	it("It returns the error unknown-error if invalid arguments are given", function(done){
		var options = {
			facebookUserId: null,
			images: testConfig.fbImages
		};

		FacebookImageSet.create(options, function(error, imageSetId){
			expect(error.message).to.equal("unknown-error");
			expect(imageSetId).to.be.null;
			done();
		});

	});

});

