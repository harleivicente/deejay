var rewire = require("rewire");
var moduleA = rewire("./moduleA");
var expect = require("chai").expect;
var sinon = require("sinon");
var spy;
describe("Testing REWIRE + SINON SPY.", function(){

	before(function(){
		spy = sinon.spy(moduleA.__get__("functionB"));
		moduleA.__set__("functionB", spy);
	})

	beforeEach(function(){
		spy.reset();
	})

	it("calls functionB once when given 1", function(done){
		moduleA.mainFn(1, function(err){
			expect(err).to.be.false;
			expect(spy.calledOnce).to.be.true;
			done();
		});
	})

	it("calls functionB twice when given 2", function(done){
		moduleA.mainFn(2, function(err){
			expect(err).to.be.false;
			expect(spy.calledTwice).to.be.true;
			done();
		});
	})

})