var expect = require('chai').expect;
var SingleImageAnimator = require('../../SingleImageAnimator');


var sampleAnalysisData = {
	// 45 samples, 1045 ms
	energy: [
		.801, .433, .797, .504, .971,
		.853, .724, .684, .432, .142,
		.328, .400, .424, .383, .287,
		.424, .687, .502, .929, .054,
		.120, .240, .329, .423, .538,
		.387, .402, .567, .489, .333,
		.238, 1.00, 1.00, 1.00, .721,
		.671, .622, .438, .539, .555,
		.234, .189, .521, .300, .666
	],
	// FAKE
	rate_of_energy: [
		.853, .724, .684, .432, .142,
		.801, .433, .797, .504, .971,
		.328, .400, .424, .383, .287,
		.671, .622, .438, .539, .555,
		.387, .402, .567, .489, .333,
		.424, .687, .502, .929, .054,
		.238, 1.00, 1.00, 1.00, .721,
		.120, .240, .329, .423, .538,
		.234, .189, .521, .300, .666
	],
	beats: [
		197, 430, 452, 708, 940
	],
	beats_intensity: [
		.4, .2, 1, .6, .72
	],
	// FAKE
	beats_intensity_rate: [
		.4, .2, 1, .6, .72
	]	
};

describe('_getSpeedIndexAtTime', function() {

	it("If called before setting up .transitionSpeedMap will return false.", function(){
		var useSpeedIncrement =  3;
		var instance = new SingleImageAnimator(sampleAnalysisData, {speedIncrements: useSpeedIncrement});
		var output = instance._getSpeedIndexAtTime(3, useSpeedIncrement);
		expect(output).to.be.false;
	})

	it("Returns false if numberOfSpeeds is not whole, greater than zero number.", function(){
		var useSpeedIncrement =  3;
		var instance = new SingleImageAnimator(sampleAnalysisData, {speedIncrements: useSpeedIncrement});
		instance.transitionSpeedMap = instance._partitionNumberLine(1,useSpeedIncrement,2);
		var outputA = instance._getSpeedIndexAtTime(2, "text");
		var outputB = instance._getSpeedIndexAtTime(2, -2);
		var outputC = instance._getSpeedIndexAtTime(2, 2.4);
		expect(outputA).to.be.false;
		expect(outputB).to.be.false;
		expect(outputC).to.be.false;
	})

	it("Returns false if request made on time out of boundaries.", function(){
		var useSpeedIncrement =  3;
		var instance = new SingleImageAnimator(sampleAnalysisData, {speedIncrements: useSpeedIncrement});
		instance.transitionSpeedMap = instance._partitionNumberLine(1,useSpeedIncrement,2);
		var outputA = instance._getSpeedIndexAtTime(1050, useSpeedIncrement);
		expect(outputA).to.be.false;
	})

	it("Returns correct result for sample data A", function(){
		var useSpeedIncrement =  3;
		var instance = new SingleImageAnimator(sampleAnalysisData, {speedIncrements: useSpeedIncrement});
		instance.transitionSpeedMap = instance._partitionNumberLine(1,useSpeedIncrement,2);

		var query = [15, 23, 40, 46, 60, 830, 414];
		var results = [1, 0, 0, 1, 1, 1, 2];

		for(var i = 0; i < query.length; i++){
			var output = instance._getSpeedIndexAtTime(query[i], useSpeedIncrement);
			expect(output).to.equal(results[i]);
		}
	})

});

