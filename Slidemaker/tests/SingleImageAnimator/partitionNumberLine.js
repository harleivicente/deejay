var expect = require('chai').expect;
var SingleImageAnimator = require('../../SingleImageAnimator');
var sampleAnalysisData = {
	energy: [10, 20, 10, 5],
	rate_of_energy: [10, 10, 10, 5],
	beats: [20],
	beats_intensity: [10],
	beats_intensity_rate: [10]	
};

describe('_partitionNumberLine', function() {

	it("It returns false if line length not a positive number greater than zero", function(){
		var instance = new SingleImageAnimator(sampleAnalysisData);
		var output = instance._partitionNumberLine("text", 3, 2);
		var outputB = instance._partitionNumberLine(-2, 3, 2);
		expect(output).to.be.false;
		expect(outputB).to.be.false;
	});

	it("It returns false if number of partitions is not positive whole number greater than zero", function(){
		var instance = new SingleImageAnimator(sampleAnalysisData);
		var output = instance._partitionNumberLine(2, "text", 2);
		var outputB = instance._partitionNumberLine(2, -2, 2);
		var outputC = instance._partitionNumberLine(2, 1.2, 2);
		var outputD = instance._partitionNumberLine(2, 0, 2);
		expect(output).to.be.false;
		expect(outputB).to.be.false;	
		expect(outputC).to.be.false;	
		expect(outputD).to.be.false;	
	});

	it("It returns false if ratio is not positive number greater than zero", function(){
		var instance = new SingleImageAnimator(sampleAnalysisData);
		var output = instance._partitionNumberLine(2, 3, "text");
		var outputB = instance._partitionNumberLine(2, 3, -1.2);
		var outputC = instance._partitionNumberLine(2, 3, 0);
		expect(output).to.be.false;
		expect(outputB).to.be.false;	
		expect(outputC).to.be.false;	
	});

	it("It returns valid response for valid sample data A", function(){
		var instance = new SingleImageAnimator(sampleAnalysisData);
		var output = instance._partitionNumberLine(1, 4, 1.4);
		expect(JSON.stringify(output)).to.equal(JSON.stringify([0.3863, 0.6622, 0.8592]));
	})

	it("It returns valid response for valid sample data B", function(){
		var instance = new SingleImageAnimator(sampleAnalysisData);
		var output = instance._partitionNumberLine(2, 6, .2);
		expect(JSON.stringify(output)).to.equal(JSON.stringify([0.0005, 0.0031, 0.0159, 0.0799, 0.3999]));		
	})

});

