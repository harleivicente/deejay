var expect = require('chai').expect;
var SingleImageAnimator = require('../../SingleImageAnimator');


var sampleAnalysisData = {
	// 45 samples, 1045 ms
	energy: [
		.801, .433, .797, .504, .971,
		.853, .724, .684, .432, .142,
		.328, .400, .424, .383, .287,
		.424, .687, .502, .929, .054,
		.120, .240, .329, .423, .538,
		.387, .402, .567, .489, .333,
		.238, 1.00, 1.00, 1.00, .721,
		.671, .622, .438, .539, .555,
		.234, .189, .521, .300, .666
	],
	// FAKE
	rate_of_energy: [
		.853, .724, .684, .432, .142,
		.801, .433, .797, .504, .971,
		.328, .400, .424, .383, .287,
		.671, .622, .438, .539, .555,
		.387, .402, .567, .489, .333,
		.424, .687, .502, .929, .054,
		.238, 1.00, 1.00, 1.00, .721,
		.120, .240, .329, .423, .538,
		.234, .189, .521, .300, .666
	],
	beats: [
		197, 430, 452, 708, 940
	],
	beats_intensity: [
		.4, .2, 1, .6, .72
	],
	// FAKE
	beats_intensity_rate: [
		.4, .2, 1, .6, .72
	]	
};

describe('_buildKeyframeSet', function() {

	it("It returns false if called before _prepareData", function(){
		var instance = new SingleImageAnimator(sampleAnalysisData, 
			{
				speedIncrements: 3,
				maxDelay: 600,
				minDelay: 5.5
			}
		);
		var outputA = instance._buildKeyframeSet([1,-1,-2]);		
		expect(outputA).to.be.false;
	})

	it("It returns false if speed combination is not valid", function(){
		var instance = new SingleImageAnimator(sampleAnalysisData, 
			{
				speedIncrements: 3,
				maxDelay: 600,
				minDelay: 5.5
			}
		);
		instance._prepareData();
		var outputA = instance._buildKeyframeSet(null);
		var outputB = instance._buildKeyframeSet([]);
		var outputC = instance._buildKeyframeSet(["text", "more"]);

		expect(outputA).to.be.false;
		expect(outputB).to.be.false;
		expect(outputC).to.be.false;
	})	

	it("It returns correct/valid response for sample data A", function(){
		var instance = new SingleImageAnimator(sampleAnalysisData, 
			{
				speedIncrements: 3,
				minDelay: 5.5,
				maxDelay: 600,
				speedRatio: 2
			}
		);
		instance._prepareData();
		var outputA = instance._buildKeyframeSet([1,-1,-2]);
		var outputB = instance._buildKeyframeSet([2, 1, -1]);

		var answerA = [
			{t: 197, i: 0.4},
			{t: 372, i: 0.4},
			{t: 430, i: 0.2},
			{t: 436, i: 0.2},
			{t: 708, i: 0.6},
			{t: 766, i: 0.6}
		];

		var answerB = [
			{t: 197, i: 0.4},
			{t: 430, i: 0.2},
			{t: 708, i: 0.6}
		];

		expect(JSON.stringify(outputA)).to.equal(JSON.stringify(answerA));
		expect(JSON.stringify(outputB)).to.equal(JSON.stringify(answerB));
	})

});

