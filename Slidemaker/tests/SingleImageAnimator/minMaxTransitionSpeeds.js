var expect = require('chai').expect;
var SingleImageAnimator = require('../../SingleImageAnimator');

var sampleA = {
	energy: [0, .1, .4, 0, .9, .2, .9], // 162 ms
	rate_of_energy: [.1, .3, -.4, .9, -.7, .7, 0],
	beats: [10, 30, 50, 55, 100],
	beats_intensity: [1, 1, 1, 1, 1],
	beats_intensity_rate: [0, 0, 0, 0, 0]	
};

var optionsA = {
	minDelay: .5,
	maxDelay: 20
};

var expectedMaxTransitionSpeedA = -2;
var expectedMinTransitionSpeedA = -3;

var sampleB = {
	energy: [0, .1, .4, 0, .9, .2, .9], // 162 ms
	rate_of_energy: [.1, .3, -.4, .9, -.7, .7, 0],
	beats: [12, 25, 54, 65, 100, 129],
	beats_intensity: [1, 1, 1, 1, 1, 1],
	beats_intensity_rate: [0, 0, 0, 0, 0, 0]	
};

var optionsB = {
	minDelay: 120,
	maxDelay: 1200
};

var expectedMaxTransitionSpeedB = 34;
var expectedMinTransitionSpeedB = 11;

describe('_defineMaxTransitionSpeed / _defineMinTransitionSpeed', function() {

	it("Calculates the proper max transition speed for sample A", function(){
		var instance = new SingleImageAnimator(sampleA, optionsA);
		var result = instance._defineMaxTransitionSpeed();
		expect(result).to.equal(expectedMaxTransitionSpeedA);
	});

	it("Calculates the proper min transition speed for sample A", function(){
		var instance = new SingleImageAnimator(sampleA, optionsA);
		var result = instance._defineMinTransitionSpeed();
		expect(result).to.equal(expectedMinTransitionSpeedA);		
	});

	it("Calculates the proper max transition speed for sample B", function(){
		var instance = new SingleImageAnimator(sampleB, optionsB);
		var result = instance._defineMaxTransitionSpeed();
		expect(result).to.equal(expectedMaxTransitionSpeedB);
	});

	it("Calculates the proper min transition speed for sample B", function(){
		var instance = new SingleImageAnimator(sampleB, optionsB);
		var result = instance._defineMinTransitionSpeed();
		expect(result).to.equal(expectedMinTransitionSpeedB);		
	});

});

