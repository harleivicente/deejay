#include "SoundManager.h"
#include "BeatDetector.h"

/*
	Sends to the output the json of:
	{	
		// Length A
		energy: [<value>, ...],
		rate_of_energy: [<value>, ...],
	
		// Length B
		beats: [<milisecond>, ...],
		beats_intensity: [<value>, ...],
		beats_intensity_rate: [<value>, ...]
	}

	@info
		Length A = (number of seconds * 44100) / 1024  	
		Each index represents (23.22 miliseconds)

		Length B = number of beats detected

	@info 
		<value> 0 - 1 with three digit precision
		(0, 0.001, ... 0.999, 1)
*/
int main(int argc, char *argv[])
{
    // Create SoundManager
    SoundManager* snd_mng = new SoundManager();
    snd_mng->load_song(argv[1]); // load song in arguments
    snd_mng->play();
    snd_mng->pause();

    // Create BeatDetector
    BeatDetector* beatdec = new BeatDetector(snd_mng);
	beatdec->audio_process();

    return 0;
}
