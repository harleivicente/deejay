Library = {};


/*
	Interpolates a range of values (0 - xmax) to another (ymin - ymax).
	Use type to define behavior: 'linear', 'quad'
*/
Library.interpolate = function(value, xmin, xmax, ymin, ymax, type) {
	var a,b,c;

	if(type == "linear"){
		a = 0;
		b = (ymax - ymin) / xmax;
		c = ymin;
	} else {
		a = -(ymax-ymin)/(Math.pow(xmax, 2));
		b = 2*(ymax-ymin)/xmax;
		c = ymin;
	}

	var result = a*Math.pow((value-xmin), 2) + b*(value-xmin) + c;
	return result.toFixed(3);
}

Library.shuffle = function(array) {
	var currentIndex = array.length, temporaryValue, randomIndex ;

	// While there remain elements to shuffle...
	while (0 !== currentIndex) {

	// Pick a remaining element...
	randomIndex = Math.floor(Math.random() * currentIndex);
	currentIndex -= 1;

	// And swap it with the current element.
	temporaryValue = array[currentIndex];
	array[currentIndex] = array[randomIndex];
	array[randomIndex] = temporaryValue;
	}

	return array;
}