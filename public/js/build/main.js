window.BuildApp = React.createClass({
    getInitialState: function() {
    	return {
    		stage: 'choose_audio',
    		audio_title: null,
    		audio_uri: null,
    		audio_length: null,
    		audio_id: null,
    		audio_time_start: null,
    		audio_time_end: null,
    		image_ids: [],
    		nof_images_min: null,
    		nof_images_max: null
    	};
	},
	render: function() {
		return <div>
			<PanelController stage={this.state.stage} main={this}/>
		</div>
	}
});

/*
	@param video_id youtube video id
	@param callback function(error, result)
		error - bool
		result - object : {length: <seconds>, title: <string>}
*/
window.youtube_get_video_info = function(video_id, callback){
	var url = "https://www.googleapis.com/youtube/v3/videos?part=contentDetails,snippet&id=" + video_id + "&key=AIzaSyDfEOaoiNNoPXCD76Gtn4fUHMQbeTiLyFc";
	$.ajax({url: url})
	.done(function(result){
		if(result.items && result.items.length != 0){
			var duration_string = result.items[0].contentDetails.duration;
			var duration = moment.duration(duration_string).asSeconds();
			var title = result.items[0].snippet.title;
			callback(false, {length: duration, title: title});
		} else {
			callback(true);
		}

	})
	.error(function(){
		callback(true);
	});
}

/*
	Request server to download the audio for a given
	youtube url.

	@param video_id
	@param callback - function(error, audio_id, invalid, audio_uri)
		error - true if unknown error occurs
		invalid - if impossible to analyze this video

*/
window.request_youtube_audio_analysis = function(video_id, callback){
	$.ajax({
		url: "http://" + window.location.host + "/audios",
		type: "POST",
		data: {
			audio_source_type: "youtube",
			config: {video_code: video_id}
		}
	})
	.done(function(result){
		if(result.status){
			callback(false, result.audio_id, false, result.audio_uri);
		} else if(result.code == "invalid"){
			callback(false, null, true, null);
		} else {
			callback(true);
		}
	})
	.error(function(){
		callback(true);
	});
}

/*
	Extracts video id from youtube url.

	@param url string
	return string | false
*/
window.extract_youtube_video_id = function(url) {
	var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
	return (url.match(p)) ? RegExp.$1 : false;
}

$.getScript("//connect.facebook.net/en_US/sdk.js")
  .done(function( script, textStatus ) {
    FB.init({
      appId      : '1803519839869180',
      cookie     : true, 
      xfbml      : true,
      version    : 'v2.2'
    });

	ReactDOM.render(<BuildApp />, document.getElementById('react-root-build'));
    
  })
.fail(function( jqxhr, settings, exception ) {});	


