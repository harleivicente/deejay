/*
	Debugger chart panel

*/

window.DebuggerChartPanel = React.createClass({
	getInitialState: function(){
		return {};
	},
	render: function(){

		return <div id="debugger-chart-panel" className="hide debugger">
			<div className="chart"></div>
			<div className="chart"></div>
			<div className="chart"></div>
			<div className="chart"></div>
		</div>
	}, 

	componentDidMount: function(){
		this.plotTimeSeries();
	},

	/*
		Graph all data sent by server via html.
		The data for a graph is parsed as:
			input(
				class='time-series-graph',
				sample-rate=<ms>,
				title= <string>,
				data=<json of array of values>
			)
	*/
	plotTimeSeries: function(){
		var instance = this;
		$("input.time-series-graph").each(function(index, item){
			item = $(item);
			instance.createTimeSeriesChart(
				item.attr("title"),
				$($("#debugger-chart-panel .chart")[index]),
				item.attr("sample-rate"),
				JSON.parse(item.attr("data"))
			);
		});
	},

	/*
		Create a chart of time.

		@param y_axis_title string
		@param container JQUERY object
		@param data_sample_rate number - I.e for sample rate of 23
			each data item represents values for 23 ms.
		@param data array - [<value>, <value>, ...]
	*/
	createTimeSeriesChart: function(y_axis_title, container, data_sample_rate, data){
		container.highcharts({
			title: null,

			chart: {
				zoomType: "x",
				panning: true,
				panKey: 'shift'
			},

		    plotOptions: {
		        series: {
		            pointStart: Date.UTC(2010, 0, 1),
		            pointInterval: 23 // one ms
		        }
		    },

			xAxis: {
				type: 'datetime',
				tickInterval: 30 * 1000,
				plotLines: [{
					color: '#FF0000',
					width: 2,
					value: 1000
				}]
			},
			yAxis: {
				title: {
					text: y_axis_title
				},
				max: 1.1
			},
			tooltip: {enabled: false},
			legend: {enabled: false},
			series: [{
				data: data
			}]
		});		

		/*
			Animate time pointer

			1262304000000 = Date.UTC(2010, 0, 1)
		*/
		var handler = container.highcharts();
		var marker = handler.xAxis[0].addPlotLine({
		  value: 1262304000000 + audio_player.currentTime * 1000,
		  width: 2,
		  color: '#333'
		});

		setInterval(function(){
		  $.extend(marker.options, {
		        value: 1262304000000 + audio_player.currentTime * 1000
		    });
		    marker.svgElem.destroy();
		    marker.svgElem = undefined;
		    marker.render();
		},1);

		/*
			Hack to make graph fit properly into container div
		*/
		setTimeout(function(){
			container.highcharts().reflow();



		},3000)
	}

});