var expect = require('chai').expect;
var SingleImageAnimator = require('../../SingleImageAnimator');

var sample = {
	energy: [0, .1, .4, 0, .9, .2, .9], // 162 ms
	rate_of_energy: [.1, .3, -.4, .9, -.7, .7, 0],
	beats: [10, 30, 50, 55, 100],
	beats_intensity: [1, 1, 1, 1, 1],
	beats_intensity_rate: [0, 0, 0, 0, 0]	
};

var resultA = [ 
	[ 12, 11, 10, 9, 8 ],
	[ 11, 10, 9, 8, 7 ],
	[ 10, 9, 8, 7, 6 ],
	[ 9, 8, 7, 6, 5 ],
	[ 8, 7, 6, 5, 4 ],
	[ 7, 6, 5, 4, 3 ],
	[ 6, 5, 4, 3, 2 ],
	[ 5, 4, 3, 2, 1 ],
	[ 4, 3, 2, 1, -1 ] 
];

var resultB = [ 
	[ -2, -3, -4],
	[ -3, -4, -5],
	[ -4, -5, -6],
	[ -5, -6, -7],
	[ -6, -7, -8]
];

describe('_transitionSpeedCombination', function() {
	
	it("Returns false if min/max transition speeds values are not set.", function(){
		var instance = new SingleImageAnimator(sample, {speedIncrements: 5});
		instance.maxTransitionSpeed = 12;
		var result = instance._defineTransitionSpeedCombinations();
		expect(result).to.be.false;
	});

	it("Returns correct answer for input A", function(){
		var instance = new SingleImageAnimator(sample, {speedIncrements: 5});
		instance.maxTransitionSpeed = 12;
		instance.minTransitionSpeed = -1;
		var result = instance._defineTransitionSpeedCombinations();
		expect(JSON.stringify(result)).to.equal(JSON.stringify(resultA));
	});

	it("Returns correct answer for input B", function(){
		var instance = new SingleImageAnimator(sample, {speedIncrements: 3});
		instance.maxTransitionSpeed = -2;
		instance.minTransitionSpeed = -8;
		var result = instance._defineTransitionSpeedCombinations();
		expect(JSON.stringify(result)).to.equal(JSON.stringify(resultB));
	});	

});

