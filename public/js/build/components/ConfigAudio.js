window.ConfigAudio = React.createClass({
	render: function() {
		var main_state = this.props.main.state;
		var player_title = main_state.audio_title;

		return <div className="panel panel-default">
			<div className="panel-heading">
				<h3 className="panel-title">Configure Audio</h3>
			</div>
			<div className="panel-body">
				<p> Your soundtrack has been processed and is ready to be used. You can use the controls down bellow to choose a section of the audio or simply use the entire audio. </p>
				<div className="audio-player">
					<p><strong> {player_title} </strong></p>
					<audio src={main_state.audio_uri} preload="auto" controls></audio>
				</div>


				<div className="trim-audio">
					<p><strong> Trim audio </strong></p>
					<div id="audio-slider" className="noUi-extended noUi-horizontal"></div>
				</div>

				<form id="length-form">
					<div className="form-group">
						<div className="input-group">
							<span className="input-group-addon"><small>Length</small></span>
							<input type="text" id="trim-length-input" className="form-control input-sm" readOnly />
						</div>
					</div>
				</form>

			</div>
			<div className="panel-footer">
				<div className="row">
					<div className="col-md-4">
						<button type="button" onClick={this.handlePrevious} className="btn btn-default">Back</button>
					</div>
					<div className="col-md-4 col-md-offset-4">
						<button type="button" onClick={this.handleNext} className="btn btn-success">Choose images</button>
					</div>
				</div>
			</div>			
		</div>
	}, 
	handlePrevious: function() {
		this.props.main.setState({
			stage: "choose_audio"
		});
	},
	handleNext: function() {
		var instance = this;
		var main_state = this.props.main.state;
		var current = $("#audio-slider")[0].noUiSlider.get();
		var base = moment("01:01:1970:00:00", "MM:DD:YYYY:mm:ss").unix();
		var end = moment("01:01:1970:" + current[1], "MM:DD:YYYY:mm:ss").unix();
		end -= base;
		if(current[0] == "00:00"){
			var start = 0;
		} else {
			var start = moment("01:01:1970:" + current[0], "MM:DD:YYYY:mm:ss").unix();
			start -= base;
		}

		// Server uses ms and start at 1 ms
		start *= 1000;
		end *= 1000;
		if(start == 0){
			start = 1;
		}

		// Get min/max number of images from server
		instance.getNeededNumberOfImages(main_state.audio_id, start, end, function(error, min, max){
			/*
				Failed to obtain min/max nof imagaes needed
			*/
			if(error){
				instance.props.main.setState({
					stage: "app_exception"
				});
			} else {
				instance.props.main.setState({
					stage: "choose_images",
					audio_time_start: start,
					audio_time_end: end,
					nof_images_min: min,
					nof_images_max: max
				});
			}
		});
	},

	/*
		Obtains min/max number of images need for 
		trimmed sountrack.

		@param audioId string
		@param start time in ms
		@param end time in ms
		@param callbck function(error, min, max)
	*/
	getNeededNumberOfImages: function(audioId, start, end, callback){
		var main = this.props.main;
		$.ajax({
			url: "http://" + window.location.host + "/neededImages",
			type: "GET",
			data: {
				audio_source_type: "youtube",
				audio_source_id: audioId,
				time_start: start,
				time_end: end
			}
		})
		.done(function(result){
			if(result.status){
				callback(false, result.min, result.max);
			} else {
				callback(true, null, null);
			}
		})
		.error(function(){
			callback(true, null, null);
		});	
	},

	componentDidMount: function(){
		var main_state = this.props.main.state;
		noUiSlider.create(document.getElementById('audio-slider'), {
			start: [0, main_state.audio_length],
			tooltips: [ true, true ],
			margin: 15,
			animate: false,
			step: 1,
			connect: true,
			range: {
				'min': [ 0 ],
				'max': [ main_state.audio_length ]
			},
			format: {
				to: function ( value ) {
					return moment.utc(value * 1000).format("mm:ss")
				},
				from: function ( value ) {
					return value;
				}
			}
		});

		$("#audio-slider")[0].noUiSlider.on("update", function(){
			var current = $("#audio-slider")[0].noUiSlider.get();
			var end = moment("01:01:1970:" + current[1], "MM:DD:YYYY:mm:ss").unix();
			if(current[0] == "00:00"){
				var length = end;
			} else {
				var start = moment("01:01:1970:" + current[0], "MM:DD:YYYY:mm:ss").unix();
				var length = end - start;
			}
			$("#trim-length-input").val(moment.utc(length * 1000).format("mm:ss"));
		});
	}
});