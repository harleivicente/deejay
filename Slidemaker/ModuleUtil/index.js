
/*
	Get the module name for a given source of
	images or audio. I.e

	youtube - YoutubeAudio,
	flickr - FlickrImageSet

	@param sourceName string
	@param sourceType string - [audio, image]

	@return string

	@throws Error

*/
function getModuleName(sourceName, sourceType){
	if(sourceType == "audio"){
		if(isValidAudioSource(sourceName)){
			return capitalizeFirstLetter(sourceName) + "Audio";
		} else {
			return new Error("invalid-source-name");
		}
	} else if(sourceType == "image") {
		if(isValidVideoSource(sourceName)){
			return capitalizeFirstLetter(sourceName) + "ImageSet";
		} else {
			return new Error("invalid-source-name");
		}		
	} else {
		return new Error("invalid-source-type");
	}
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function isValidAudioSource(string) {
	return validAudioSource.indexOf(string) >= 0;
}

function isValidVideoSource(string) {
	return validImageSources.indexOf(string) >= 0;
} 

module.exports.getModuleName = getModuleName;