window.PlayerApp = React.createClass({
	getInitialState: function(){
		var dev_input = $("input[name='dev-mode']");
		var dev_mode = dev_input && dev_input.length > 0;

		// Fetch soundtrack trim locations from html & convert into seconds
		var trim_start_ms = JSON.parse($("input[name='trim-start']").val());
		var trim_end_ms = JSON.parse($("input[name='trim-end']").val());
		var trim_start = Math.round(trim_start_ms/1000);
		var trim_end = Math.round(trim_end_ms/1000);
			
		return {
			dev: dev_mode,
			loading: true,
			error: false,
			slide_missing: false,
			trim_start: trim_start,
			trim_end: trim_end,
			checkpoints: {
				audio: {text: "Loading audio file", ready: null},
				audio_analysis: {text: "Loading audio analysis", ready: null},
				images: {text: "Loading images", ready: null}
			},
			debugger: null
		}
	},

	reloadApp: function() {
		window.location = "/build";
	},

	render: function(){	
		if(this.state.dev){
			var dev_module = <Debugger main={this} />
			var dev_chart_panel = <DebuggerChartPanel />
		} else {
			var dev_module = "";
			var dev_chart_panel = "";
		}

		if(this.state.loading) {
			var content = <Loading main={this}/>
		} else if(this.state.error && this.state.slide_missing) {
			var content  = <div className="panel panel-default">
				<div className="panel-heading">
					<h3 className="panel-title">Unable to load this slide.</h3>
				</div>
				<div className="panel-body">
					<p>
						The slide you are requesting does not exist. Either it was deleted or you were given the wrong link.
					</p>
				</div>
				<div className="panel-footer">
					<div className="row">
					<div className="col-md-4 col-md-offset-8">
							<button src="/home" onClick={this.reloadApp} className="btn btn-success">Reload app</button>
						</div>
					</div>
				</div>
			</div>
		} else if(this.state.error) {
			var content = <AppException />
		} else {
			var content = <Player dev={this.state.dev}/>
		}

		return <div>
			{dev_module}
			{dev_chart_panel}
			{content}
		</div>
	},

	componentDidMount: function(){
		window.manager = new Manager(this.state.dev, this);
		var instance = this;

		async.waterfall([
			function(cb){
				setTimeout(function(){
					instance.startCheckpoint("audio");
					manager.load_audio(cb);
				},300);
			},
			function(cb){
				setTimeout(function(){
					instance.finishCheckpoint("audio");
					instance.startCheckpoint("audio_analysis");
					cb(null);
				},300);
			},
			function(cb){
				setTimeout(function(){
					instance.finishCheckpoint("audio_analysis");
					instance.startCheckpoint("images");	
					manager.load_images(cb);	
				},300);

			}, function(cb){
				instance.finishCheckpoint("images");
				cb();
			}
		], function(error){
			if(error){
				manager.check_server_side_error(function(removed){
					if(removed){
						instance.setState({
							loading: false,
							error: true,
							slide_missing: true
						});
					} else {
						instance.setState({
							loading: false,
							error: true,
							slide_missing: false
						});
					}
				});
			} else {
				instance.setState({
					loading: false,
					error: false
				});

				/*
					From this point on REACT should 
					not interfere with control 
					anymore. Manager will be
					responsible for building
					canvas and setting up
					events.
				*/
				instance.openPlayerAnimation(function(){

					// Setup audio volume controller loop
					setInterval(function(){
						instance.audioFadingLoop();
					}, 100);

					manager.construct();
				});

			}
		});
	},

	startCheckpoint: function(name){
		var current = this.state;
		current.checkpoints[name].ready = false;
		this.setState(current);
	},

	finishCheckpoint: function(name){
		var current = this.state;
		current.checkpoints[name].ready = true;
		this.setState(current);
	},

	openPlayerAnimation: function(callback) {
		$("body").css({overflow: "hidden"});
		$(".page-wrap").css({
			paddingTop: 0,
			paddingBottom: 0,
			marginBottom: 0
		});
		$("#react-root-player").parent().removeClass("col-md-8 col-md-offset-2");
		$("#react-root-player").parent().parent().removeClass("row");
		$(".page-wrap > div").removeClass("container");
		$("body#body").css("background-image", "none");
		$("body").css("background-color", "#000");

		/*
			Try deleting header and footer
		*/
		$("#app-navbar").remove();
		$(".site-footer").remove();
		callback();

		if(this.state.dev){
			$(".debugger").removeClass("hide");
		}	
	},

	/*
		Fades audio in and out. Uses volume attribute of
		audio tag.
	
		@info Fails if audio is shorter then twice the window used
		@info window of 5 seconds
	*/
	audioFadingLoop: function(){
		var falloff_duration = 2;
		var duration = this.state.trim_end - this.state.trim_start;

		// @param x - 0 to 1
		// @return y - 0 to 1
		var fade_in = function(x){
			if(x < 0) {
				return 0;
			} else if(x > 1) {
				return 1;
			} else {
				return Math.pow(x, 2);
			}
		}

		// @param x - 0 to 1
		// @return y - 0 to 1
		var fade_out = function(x){
			if(x < 0) {
				return 1;
			} else if(x > 1) {
				return 0;
			} else {
				return Math.pow((x-1), 2);
			}
		}		
		if(duration > (2 * falloff_duration)){

			// Fade in
			if(audio_player.currentTime - this.state.trim_start <= falloff_duration){
				var input = (audio_player.currentTime - this.state.trim_start)/falloff_duration;
				var output = fade_in(input);
				audio_player.volume = output;
			// During song
			} else if(this.state.trim_end - audio_player.currentTime > falloff_duration){
				audio_player.volume = 1;
			// Fade out
			} else {
				var input = (audio_player.currentTime - this.state.trim_end + falloff_duration)/falloff_duration;
				var output = fade_out(input);
				audio_player.volume = output;
			}

		}
	}

});

$.getScript("//connect.facebook.net/en_US/sdk.js")
  .done(function( script, textStatus ) {
    FB.init({
      appId      : '1803519839869180',
      cookie     : true, 
      xfbml      : true,
      version    : 'v2.2'
    });
	ReactDOM.render(<PlayerApp />, document.getElementById('react-root-player'));
  })
.fail(function( jqxhr, settings, exception ) {});	